import matplotlib.pyplot as plt
from preprocessing import *
import tensorflow as tf

import pickle
import os
import pyarrow.parquet as pq
import dill

from monitoring import r2, ccc, ccc_numpy

METRICS = {"r2": r2, "ccc": ccc}


def feature_mapper(features, sub_len, c_col, cols):
    f = np.insert(features, (c_col)*((2*sub_len)-1) + sub_len-1, np.nan)
    f = f.reshape(len(cols), 2*sub_len - 1).T
    out = pd.DataFrame(f, columns=cols, index=np.arange(1-sub_len, sub_len))
    return out


def scoreClassifier(obs, pred):
    confusion = sklearn.metrics.confusion_matrix(obs, pred)
    conf_df = pd.DataFrame(confusion, columns=['detected_as_dry', 'detected_as_wet'], index=['in_fact_dry', 'in_fact_wet'])
    scores = {'samples': len(obs),
     'rainy_obs': int(np.sum(obs == 1)),
     'undetected_rainy': int(confusion[1, 0]),
     'dry_obs': int(np.sum(obs == 0)),
     'undetected_dry': int(confusion[0, 1]),
     'rain_detect': confusion[1,1]/(confusion[1,1] + confusion[1,0]),
     'dry_detect': confusion[0,0]/(confusion[0,1] + confusion[0,0])
     }
    return conf_df, pd.Series(scores.values(), index=scores.keys())



def plotAligned(ser_list):
    fig, ax = plt.subplots(len(ser_list), sharex=True, sharey=True)
    for k, var in enumerate(ser_list):
        ax[k].plot(var)
        ax[k].set_title(var.name)
    return fig


def get_wet_index(folder_path, target_ID):
    frame = pd.read_parquet(folder_path)
    wet_index = frame.index[frame.loc[:, target_ID] == 1]
    return wet_index


class Analyze:
    def __init__(
        self,
        run,
        results_p="Results/",  # results path should be on same folder level as Data path
        data_p=None,
        score_file="all_scores",
        generator_config="generator_config",
        model_suff="_model",
        history_suff="_history",
        scatter_suff="_pred_scatter",
        ser_suff="_pred_ser",
        summary_suff="_summary",
        id_file="ID",
        scale_file="scales",
        trafo_file="trafo",
        RR_folder="RR",
        rH_folder="rH",
        train_index="train_index",
        val_index="validation_index",
        s_rate="10min",
    ):

        self.results_p = results_p
        if data_p is None:
            self.data_p = os.path.join(results_p, "../DWDData")
        else:
            self.data_p = data_p

        self.RR_p = os.path.join(self.data_p, RR_folder)
        self.rH_p = os.path.join(self.data_p, rH_folder)
        self.run_p = os.path.join(self.results_p, run)
        self.model_suff = model_suff
        self.history_suff = history_suff
        self.scatter_suff = scatter_suff
        self.ser_suff = ser_suff
        self.scatter_suff = scatter_suff
        self.summary_suff = summary_suff
        self.X = None
        self.Y = None
        self.P = None
        self.M = None
        self.s_rate = s_rate
        try:
            self.scores = pd.read_csv(os.path.join(self.run_p, score_file + ".csv"))
            self.IDx = pd.read_csv(os.path.join(self.run_p, id_file + "x.csv"))
            self.IDy = pd.read_csv(os.path.join(self.run_p, id_file + "y.csv"))
            # backwards compatibility for simple scaling runs:
            if os.path.exists(os.path.join(self.run_p, scale_file + ".csv")):
                self.trafos = pd.read_csv(os.path.join(self.run_p, scale_file + ".csv"))
                self.trafos = self.trafos.set_index(self.trafos.columns[0]).squeeze()
            else:
                with open(os.path.join(self.run_p, trafo_file + ".pickle"), 'rb') as f:
                    self.trafos = dill.load(f)

            self.train_i = (
                pd.read_csv(os.path.join(self.run_p, train_index + ".csv"))
                .squeeze()
                .values
            )
            self.train_i = pd.DatetimeIndex(self.train_i)
            self.val_i = (
                pd.read_csv(os.path.join(self.run_p, val_index + ".csv"))
                .squeeze()
                .values
            )
            self.val_i = pd.DatetimeIndex(self.val_i)
        except FileNotFoundError:
            print(
                "Run Folder is not a valid Experiment. Running Analytics in Dummy Mode."
            )
        try:
            self.generator_config = pd.read_csv(
                os.path.join(self.run_p, generator_config + ".csv"), index_col=0
            )
            self.generator_config = self.generator_config.to_dict(orient="records")[0]
        except FileNotFoundError:
            print(
                "No data generator specification found. Using plain spacial predictors."
            )
            self.generator_config = {
                "pred_mode": "spacial",
                "sub_len": 1,
                "center_target": False,
            }

        self.miss_mask = pd.DataFrame([])  # mask of artificial data corruption

    def fill_prediction(self, fill_val):
        filled = pd.DataFrame({self.P.columns[0]: fill_val}, index=self.Y.index.join(self.P.index, how='outer'))
        filled.loc[self.P.index, :] = self.P.squeeze()
        self.P = filled

    def summary(self, mod_num, pr=True):
        with open(
            os.path.join(self.run_p, str(mod_num) + self.summary_suff + ".txt"), "r"
        ) as f:
            summ = f.read()
        if pr:
            print(summ)
            return None
        else:
            return summ

    def predict_with_dropout(self, mod_num, drop_rate=None):
        if isinstance(mod_num, (int, np.int64)):
            self.M = tf.keras.models.load_model(
                os.path.join(self.run_p, str(mod_num) + "_model"),
                custom_objects=METRICS,
            )

        self.M.layers[0].rate = drop_rate
        self.P = self.M(self.G[0][0], training=True).numpy()
        self.P = pd.DataFrame(
            self.P,
            columns=[self.G.y_data.name],
            index=self.G.y_data.index[self.G.gen_index],
        )
        self.P = transformer(self.trafos, self.P, mode="bwd")

    def plot_dropout_predictions(self, mod_num=None, rates=[]):
        fig, ax = plt.subplots(len(rates), sharex=True, sharey=True)
        for k, rate in enumerate(rates):
            if mod_num is None or isinstance(mod_num, int):
                self.predict_with_dropout(mod_num, drop_rate=rate)
            else:
                self.predict_with_dropout(mod_num[k], drop_rate=rate)
            self.fill_prediction(0)
            ax[k].plot(self.Y, label="obs")
            ax[k].plot(self.P, label="pred")
            if k == 0:
                ax[k].legend()

            ax[k].set_title(str(100 * rate) + "% missing")

        return fig

    def load_model(self, mod_num, best=None):
        self.M = tf.keras.models.load_model(
            os.path.join(self.run_p, str(mod_num) + "_model"), custom_objects=METRICS
        )
        if best:
            self.M.load_weights(os.path.join(self.run_p, '_'.join([str(mod_num),best])))

    def predict_data(self, mod_num=None):
        if mod_num == "linear":
            self.P = self.Y.rolling(3, center=True).apply(
                func=lambda x: ((x[0] + x[-1]) * 0.5), raw=True
            )
        else:
            if isinstance(mod_num, (int, np.int64)):
                self.M = tf.keras.models.load_model(
                    os.path.join(self.run_p, str(mod_num) + "_model"),
                    custom_objects=METRICS,
                )

            self.P = pd.DataFrame(
                self.M.predict(self.G),
                columns=[self.G.y_data.name],
                index=self.G.y_data.index[self.G.gen_index],
            )
            self.P = transformer(self.trafos, self.P, mode="bwd")
        return 0

    def calc_scores(self, rain_only=True):
        if rain_only:
            sample_i = self.Y[(self.Y != 0).values].index
            sample_i = sample_i.join(self.P.index, how="inner")
        else:
            sample_i = self.P.index.join(self.Y.index, how="inner")

        sample_i = self.G.x_data.index[self.G.gen_index].join(sample_i, how='inner')

        obs = self.Y.loc[sample_i, :].squeeze()
        pred = self.P.loc[sample_i, :].squeeze()

        na_i = pred.isna() | obs.isna()
        obs = obs.drop(na_i[na_i].index)
        pred = pred.drop(na_i[na_i].index)
        ccc_test = ccc_numpy(obs, pred)
        r2_test = 1 - np.sum((obs - pred) ** 2) / np.sum((obs - np.mean(obs)) ** 2)
        mse_test = np.mean((obs - pred) ** 2)

        return pd.Series([mse_test, ccc_test, r2_test], index=["mse", "ccc", "r2"])

    def plot_reg(self, mod_num=None):
        if mod_num:
            self.predict_data(mod_num)
        fig = plt.scatter(self.Y.loc[self.P.index, :].values, self.P.values)
        m = max(self.Y.max()[0], self.P.max()[0])
        plt.plot([0, m], [0, m], color="black")
        plt.axis("square")
        plt.xlabel("observation")
        plt.ylabel("prediction")
        return fig

    def plot_PvO(self, mod_num=None):
        if mod_num is not None:
            self.predict_data(mod_num)
        fig = plt.plot(self.Y, label="Observation")
        plt.plot(self.P, label="Prediction")
        plt.title(mod_num)
        return fig

    def plot_predictors(self, plot_p=True):
        fig, ax = plt.subplots(self.X.shape[1], sharex=True, sharey=True)
        for k, var in enumerate(self.X.columns):
            ax[k].plot(self.X[var], label="observation")
            ax[k].set_title(var)
            if var == self.P.columns[0] and plot_p:
                ax[k].plot(self.P[var], label="prediction")
                ax[k].legend()
        return fig

    def plot_hist(self, mod_num, metric="all"):
        with open(
            os.path.join(self.run_p, str(mod_num) + self.history_suff + ".pickle"), "rb"
        ) as f:
            hist = pickle.load(f)
        if metric == "all":
            to_plot = [x.split("_")[1] for x in hist.keys() if x.split("_")[0] == "val"]
            fig, axes = plt.subplots(len(to_plot), 1)
            for p in range(0, len(to_plot)):
                axes[p].plot(hist[to_plot[p]], label="training")
                axes[p].plot(hist["val_" + to_plot[p]], label="validation")
                axes[p].set_ylabel(to_plot[p])
                if p == 0:
                    axes[p].legend()
        else:
            fig = plt.plot(hist[metric], label="training")
            plt.plot(hist["val_" + metric], label="validation")
            plt.set_ylabel(metric)

        return fig

    def load_data(
        self,
        years,
        drop_ts=["any"],
        use_index=None,
        corrupt=False,
        load_mode: list = None,
    ):

        if load_mode:
            xDATA_cols = np.array(load_mode)
            yDATA_cols = np.array([])
        else:
            xDATA_cols = self.IDx.iloc[:, 0].astype(str).values
            yDATA_cols = self.IDy.iloc[:, 0].astype(str).values

        x_frame = pd.DataFrame(columns=xDATA_cols)
        y_frame = pd.DataFrame(columns=yDATA_cols)
        #var_paths = {"RR": self.RR_p, "rH": self.rH_p}

        for y in years:
            x_year = pd.DataFrame(columns=xDATA_cols)
            y_year = pd.DataFrame(columns=yDATA_cols)
            for type in os.listdir(self.data_p):
                x_IDs_t = [x for x in xDATA_cols if x.split("_")[1] == type]
                y_IDs_t = [x for x in yDATA_cols if x.split("_")[1] == type]
                if len(x_IDs_t) > 0:
                    # load variable
                    path = os.path.join(self.data_p, type, y + ".parquet")
                    access = pq.ParquetFile(path)
                    x_IDs = [
                        k.name for k in access.schema if k.name + "_" + type in x_IDs_t
                    ]
                    x_data = pd.read_parquet(path, columns=x_IDs)
                    y_IDs = [
                        k.name for k in access.schema if k.name + "_" + type in y_IDs_t
                    ]
                    y_data = pd.read_parquet(path, columns=y_IDs)

                    if corrupt:
                        corr_mask = self.miss_mask.loc[
                            x_data.index, [x + "_" + type for x in x_data.columns]
                        ]
                        x_data[corr_mask.values] = np.nan

                    # add predictors and observations to data
                    x_year[[x + "_" + type for x in x_IDs]] = x_data[x_IDs]
                    y_year[[x + "_" + type for x in y_IDs]] = y_data[y_IDs]

            x_frame = x_frame.append(x_year)
            y_frame = y_frame.append(y_year)

        x_frame = x_frame.loc[~x_frame.index.duplicated(), :]
        y_frame = y_frame.loc[~y_frame.index.duplicated(), :]
        self.X = x_frame
        self.Y = y_frame

        if not load_mode:
            x_frame = x_frame.resample(self.s_rate).asfreq()
            y_frame = y_frame.resample(self.s_rate).asfreq()
            if use_index is None:
                if "y" in drop_ts:
                    ref_frame = y_frame
                elif "any" in drop_ts:
                    ref_frame = pd.concat([x_frame, y_frame], axis=1)
                    ref_frame = ref_frame.loc[:, ~ref_frame.columns.duplicated()]
                else:
                    ref_frame = pd.DataFrame([])

                i_map = get_index_map(
                    ref_frame, self.generator_config["sub_len"] + 1, s_rate=self.s_rate
                )

                i_map = get_index_map(
                    ref_frame,
                    -(
                        bool(self.generator_config["center_target"])
                        * (int(self.generator_config["sub_len"]) + 1)
                    ),
                    s_rate=self.s_rate,
                    in_map=i_map,
                )
                if "wet" in drop_ts:
                    i_map = get_index_map(
                        ref_frame, cond="wet", s_rate=self.s_rate, in_map=i_map
                    )
                if "rainy" in drop_ts:
                    i_map = get_index_map(
                        ref_frame,
                        cond="wet@" + y_frame.columns[0].split("_")[0],
                        s_rate=self.s_rate,
                        in_map=i_map,
                    )
            else:
                ref_frame = pd.concat([x_frame, y_frame], axis=1)
                ref_frame = ref_frame.loc[:, ~ref_frame.columns.duplicated()]
                i_map = pd.Series(
                    np.arange(len(ref_frame.index)), index=ref_frame.index
                )
                i_map = i_map[ref_frame.index.join(use_index, how="inner")]
            ref_frame = transformer(self.trafos, ref_frame)
            x_frame = transformer(self.trafos, x_frame)
            y_frame = transformer(self.trafos, y_frame)
            if not self.generator_config["pred_mode"] == "spacial":
                self.G = TSGen(
                    x_data=ref_frame,
                    y_data=y_frame.squeeze(),
                    batch_size=len(i_map),
                    gen_index=i_map,
                    **self.generator_config
                )
            else:
                self.G = TSGen(
                    x_data=x_frame,
                    y_data=y_frame.squeeze(),
                    batch_size=len(i_map),
                    gen_index=i_map,
                    **self.generator_config
                )
        return 0


if __name__ == "__main__":



    # new trafo test:
    #A = Analyze(run="currentRun/warbBench5600Linear", RR_folder="RR")
    #A.load_data(['2020'], drop_ts=['any', 'rainy'])
    #A.load_model(0)
    #A.predict_data()



    # linear
    use_index_dtw = pd.read_csv('/home/luenensc/PyPojects/NeuraLab/Results/classification/rain_i_dtw_bench.csv')
    use_index_dtw = pd.DatetimeIndex(use_index_dtw['0'].values)


    use_index = pd.read_csv('/home/luenensc/PyPojects/NeuraLab/Results/classification/rain_i.csv')
    use_index = pd.DatetimeIndex(use_index['0'].values)

    #DropOutCasc = Analyze(run="currentRun/pipeBench", RR_folder="RR")
    #DropOutCasc.load_data(["2020"], use_index=use_index)
    #DropOutCasc.predict_data(0)
    #DropOutCasc.calc_scores()
    #DropOutCasc.fill_prediction(0)
    #DropOutCasc.plot_PvO()



    DropOutCasc = Analyze(run="currentRun/pipeBench", RR_folder="RR")
    DropOutCasc.load_data(["2020"], use_index=use_index)
    DropOutCasc.predict_data(0)
    DropOutCasc.calc_scores()
    DropOutCasc.fill_prediction(0)
    #DropOutCasc.predict_data('linear')
    #DropOutCasc.calc_scores()


    # plot predictors with 5600
    A = Analyze(run="1h_Flat_r.25_RR_5600_10Y_dropOut", RR_folder="RR")
    A.load_data(["2020"], drop_ts=["any", "rainy"])
    A.predict_data(0)
    A.plot_predictors(False)

    # load rH study
    RROnly = Analyze(run="currentRun/1H_DropoutCascade_RR_only6312", RR_folder="RR")
    RRrH = Analyze(run="currentRun/1H_RR_rH_only6312", RR_folder="RR")
    RRPermRH = Analyze(run="currentRun/1H_RR_rHPerm_6312only", RR_folder="RR")
    #RROnly.load_data(["2020"], drop_ts=["any", "rainy"])
    RRrH.load_data(["2020"], drop_ts=["any", "rainy"])
    RRPermRH.load_data(["2020"], drop_ts=["any", "rainy"])
    RRrH.predict_data(1)
    RRPermRH.predict_data(0)
    RRrH.calc_scores()
    RRPermRH.calc_scores()


    # Drop Out
    DropOutCasc = Analyze(run="1H_DropoutCascade_5600_better", RR_folder="RR")
    DropOutCasc.load_data(["2020"], drop_ts=["any", "rainy"])
    DropOutCasc.predict_data(1)
    DropOutCasc.calc_scores()
    DropOutCasc.fill_prediction(0)
    DropOutCasc.plot_PvO()
    DropOutCasc.plot_predictors()

    # 3) drop out stability:
    DropOutCasc.plot_dropout_predictions([0, 1, 2], [0, 0.1, 0.2])
    DropOutCasc.plot_dropout_predictions([3, 4, 5], [0.3, 0.4, 0.5])

    # 4) scores for drop cascade:
    score_frame = pd.DataFrame([])
    DropOutCasc.predict_data(0)
    score_frame["rate_0"] = DropOutCasc.calc_scores()
    DropOutCasc.predict_with_dropout(0, 0.1)
    score_frame["rate_0.1"] = DropOutCasc.calc_scores()
    for k in range(2, 6):
        DropOutCasc.predict_with_dropout(k, k * 0.1)
        score_frame["rate_" + str(round(k * 0.1, 1))] = DropOutCasc.calc_scores()



    # A.plot_reg()
    # A.plot_PvO()
    # x_data = pd.DataFrame({'a': [1, 2, 3, 4, 5, 6], 'b': [2, 3, 4, 5, 6, 7]})
    # y_data = pd.Series([2, 4, 6, 8, 10, 12], name='y')
    # gen_index = np.arange(2, 5)
    # TSG=TSGen(x_data, y_data, center_target=True, sub_len=2, gen_index=gen_index, pred_mode='embedded', pred_id='a', miss_val=-1)
    # TSG[0]
