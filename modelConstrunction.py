import tensorflow as tf


def checkpointer(filename: str):
    from keras import callbacks

    return callbacks.ModelCheckpoint(
        filename,
        monitor="val_loss",
        verbose=1,
        save_best_only=True,
        save_weights_only=False,
        mode="auto",
        period=1,
    )


def early_stopping(patience: int = 5, min_delta=0, monitor="val_loss"):

    return tf.keras.callbacks.EarlyStopping(
        monitor=monitor,
        min_delta=min_delta,
        patience=patience,
        verbose=1,
        mode="auto",
        baseline=None,
        restore_best_weights=True,
    )


def reduce_learning_rate(factor: float = 0.5, patience: int = 3):

    return tf.keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss",
        factor=factor,
        patience=patience,
        verbose=1,
        mode="auto",
        min_delta=0.0001,
        cooldown=0,
        min_lr=1e-6,
    )


def make_model(
    layer_list,
    input_dim,
    loss="mean_squared_error",
    monitors=["mae"],
    seed=None,
    optimizer=tf.keras.optimizers.Adam(lr=1e-3),
):

    tf.keras.backend.clear_session()
    initializer = tf.keras.initializers.glorot_normal(seed=seed)
    model = tf.keras.models.Sequential()
    n = 0
    for lay in layer_list:
        if isinstance(lay, dict):
            lay_typ = lay["type"].pop
            if "initializer" in lay.keys():
                lay["kernel_initializer"] = initializer
                lay_dict = lay
        if isinstance(lay, str):
            split = lay.find("_")
            lt = lay[split + 1 :]
            larg = lay[:split]
            if lt == "drop":
                lay_typ = "Dropout"
                lay_dict = {"rate": float(larg)}
                lay_dict.update({"seed": seed})

        if isinstance(lay, int):
            lay_typ = "Dense"
            lay_dict = {"units": lay}
            lay_dict.update({"kernel_initializer": initializer})
            if n == 0:
                lay_dict.update({"input_dim": input_dim})
            if n == (len(layer_list) - 1):
                lay_dict.update({"activation": "linear"})
            else:
                lay_dict.update({"activation": "relu"})
        n += 1

        model.add(getattr(tf.keras.layers, lay_typ)(**lay_dict))

    model.compile(optimizer=optimizer, loss=loss, metrics=monitors)
    return model
