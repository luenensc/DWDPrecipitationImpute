import scipy.spatial
#from preprocessing import *
import numpy as np
import pandas as pd
import os
import tensorflow as tf
import sklearn
import scipy
from functools import partial
from tqdm import tqdm
import imsdtw
#from analytics import Analyze

dtw_map=imsdtw.IMSDTW()

class TSGen(tf.keras.utils.Sequence):
    def __init__(
        self,
        x_data,
        y_data,
        sub_len=1,
        batch_size=None,
        gen_index=np.array([]),
        pred_mode="forecast",
        pred_id=None,
        miss_val=None,
        center_target=False,
        flatten=False,
        rm_target_from_flat=True,
        shuffle=False,
        weights=None,
        warp=False,
        warp_fill=None,
        warp_range=4
    ):

        self.shuffle = shuffle
        self.x_data = x_data  # dataframe, containing source data
        self.y_data = y_data  # series, containing target data
        self.sub_len = int(sub_len)
        if gen_index is None:
            self.gen_index = np.arange([0, x_data.shape[0]])
        else:
            self.gen_index = gen_index

        if center_target and (pred_mode == "embedded"):
            self.future_increment = int(self.sub_len - 1)

        else:
            self.future_increment = 0

        self.batch_size = 0
        self.batch_num = 0
        self.last_batch = 0
        self.set_batch_size(batch_size or len(self.gen_index))
        self.pred_mode = pred_mode
        if pred_id in x_data.columns:
            self.pred_id = self.x_data.columns.get_loc(pred_id)
        else:
            self.pred_id = None

        self.miss_val = miss_val
        self.flatten = bool(flatten)
        self.rm_target_from_flat = bool(rm_target_from_flat)
        self.center_target = center_target
        self.weights = weights
        self.batch_indexer = pd.Series(np.arange(len(gen_index),), index=gen_index)

        self.warp = warp
        self.warp_fill = warp_fill
        self.warp_storage = None
        self.warp_range = warp_range

        if self.warp_fill == 'linear':
            self.warp_fill = ((self.y_data.shift(1) + self.y_data.shift(-1))*.5).values

        if self.warp:
            if self.shuffle:
                print('No shuffle available when warping is active!')
            # storing is cheaper then warping! (kind of)
            if self.warp_storage is None:
                X_w, rep_i = self.get_X(self.sub_len, self.future_increment, self.gen_index, self.pred_id, self.miss_val)
                X_w = self.warp_X(X_w, self.gen_index)
                X_w = X_w.reshape(
                    len(gen_index), self.sub_len + self.future_increment, self.x_data.shape[1]
                )
                self.warp_storage = X_w

    def __len__(self):
        return int(self.batch_num)

    def __getitem__(self, item):
        batch_i = self.get_batch(item)
        X, rep_i = self.get_X(self.sub_len, self.future_increment, batch_i, self.pred_id, self.miss_val)

        if self.warp:
            if self.shuffle:
                print('No shuffle available when warping is active!')
            if self.warp_storage is None:
                X = self.warp_X(X, batch_i)
            else:
                X = self.warp_storage[self.batch_indexer[batch_i], :, :]

        if self.flatten:
            if self.pred_mode == "embedded" and self.rm_target_from_flat:
                X = np.delete(X, rep_i)
                X = X.reshape(
                    len(batch_i),
                    ((self.sub_len + self.future_increment) * self.x_data.shape[1]) - 1,
                )
            else:
                X = X.reshape(
                    len(batch_i),
                    (self.sub_len + self.future_increment) * self.x_data.shape[1],
                )
        else:
            X = X.reshape(
                len(batch_i), self.sub_len + self.future_increment, self.x_data.shape[1]
            )


        # target
        Y = self.y_data.iloc[batch_i].values
        if self.weights is None:
            return X, Y
        else:
            W = self.weights[batch_i]
            return X, Y, W


    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.gen_index)
        pass


    def set_batch_size(self, bs):
        self.batch_size = int(bs)
        self.batch_num = int(np.ceil(len(self.gen_index) / self.batch_size))
        self.last_batch = int(len(self.gen_index) % self.batch_size)
        pass


    def get_X(self, sub_len, future_increment, batch_i, pred_id, miss_val):
        x_features = self.x_data.shape[1]
        X = np.empty(
            (sub_len + future_increment) * len(batch_i) * self.x_data.shape[1]
        )
        X.fill(np.nan)
        # return subsample with target ahead
        if self.pred_mode == "forecast":
            for k, i in enumerate(batch_i):
                X[
                k
                * sub_len
                * self.x_data.shape[1]: (k + 1)
                                        * sub_len
                                        * self.x_data.shape[1]
                ] = np.ravel(self.x_data.iloc[i - sub_len: i, :].values)
        # return subsample with last period containing target and target surrounding:
        if self.pred_mode == "embedded":
            for k, i in enumerate(batch_i):
                X[
                k
                * (future_increment + sub_len)
                * self.x_data.shape[1]: (k + 1)
                                        * (sub_len + future_increment)
                                        * self.x_data.shape[1]
                ] = np.ravel(
                    self.x_data.iloc[
                    i - sub_len + 1: i + 1 + future_increment, :
                    ].values
                )
            # replace value-to-be-predicted
            base_i = (sub_len - 1) * x_features + pred_id
            rep_i = base_i + (
                    (sub_len + future_increment) * x_features
            ) * np.arange(0, batch_i.shape[0])
            X[rep_i] = miss_val
        # return surrounding only
        if self.pred_mode == "spacial":
            X = self.x_data.iloc[batch_i, :].values
            self.flatten = True
            self.sub_len = 1

        return X, rep_i


    def warp_X(self, X, batch_i):
        # warp mode assumes center and embedded mode!
        warp_range = self.warp_range*self.sub_len
        s = X.shape
        X = X.reshape(
            len(batch_i), self.sub_len + self.future_increment, self.x_data.shape[1]
        )
        tw = [c for c in self.x_data.columns if ((c != self.y_data.name) and (c.split('_')[-1] == 'RR'))]
        tw = [self.x_data.columns.get_loc(x) for x in tw]
        for to_warp in tw:
            for k, b_i in enumerate(batch_i):
                temp_range = min(warp_range, b_i)
                source = self.x_data.iloc[b_i - temp_range:b_i + temp_range - 1, to_warp].values.copy()
                target = self.y_data[b_i - temp_range:b_i + temp_range - 1].values.copy()
                if target[temp_range]!=self.y_data[b_i]:
                    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                    print('!!!!!!!!INFORMATION LEAKAGE WARNING!!!!!!!!!')
                    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                    print(b_i)
                target[temp_range] = self.warp_fill[b_i]
                dist, map = dtw_map(target, source)
                if np.isnan(dist):
                    src_mask = np.isnan(source)
                    trg_mask = np.isnan(target)
                    src_na_i = np.nonzero(src_mask)[0]
                    trg_na_i = np.nonzero(trg_mask)[0]
                    na_i = np.concatenate([src_na_i, trg_na_i])
                    na_i.sort()
                    w = na_i - temp_range
                    w_bef = w[w < 0]
                    w_aft = w[w > 0]
                    if len(w_bef) > 0:
                        w_start = w_bef[-1] + temp_range + 1
                    else:
                        w_start = 0
                    if len(w_aft > 0):
                        w_end = w_aft[0] + temp_range
                    else:
                        w_end = None

                    source = source[w_start:w_end]
                    target = target[w_start:w_end]
                    temp_range = temp_range - w_start
                    dist, map = dtw_map(target, source)

                map_frame = pd.DataFrame(map, columns=['target', 'source'])
                map_frame['source'] = source[map_frame['source']]
                warped = getattr(map_frame.groupby('target'), 'max')().squeeze().values
                X[k][:, to_warp] = warped[temp_range - self.sub_len:temp_range + self.sub_len - 1]

        X = X.reshape(s)
        #self.warp_storage = X.copy()
        return X


    def get_batch(self, item):
        if (item == self.batch_num - 1) & (self.last_batch > 0):
            batch_i = self.gen_index[-int(self.last_batch) :]
        # elif regular epoch
        else:
            batch_i = self.gen_index[
                int(item * self.batch_size) : int((item + 1) * self.batch_size)
            ]
        return batch_i

    def target_to_binary(self, cond=lambda x: x > 0, pos_class=1, neg_class=0):
        self.y_data = pd.Series(np.where(cond(self.y_data), 1, 0), index=self.y_data.index, name=self.y_data.name)



def get_index_map(data, cond, s_rate, in_map=np.array([])):
    if isinstance(cond, (np.int64, int)):
        s = np.sign(cond)
        range = s * cond * pd.Timedelta(s_rate)
        c_check = data.notna().all(axis=1)
        c_check = (
            c_check.iloc[::s].rolling(pd.Timedelta(range)).sum().iloc[::s] == s * cond
        )
    elif isinstance(cond, str):
        cond = cond.split("@")
        if len(cond) == 1:
            if cond[0] == 'wet':
                rr_id = [x for x in data.columns if x.split("_")[1] == "RR"]
                c_check = (data.loc[:, rr_id] > 0).any(axis=1)
        else:
            if cond[0] == 'wet':
                c_check = data[cond[1] + "_RR"] > 0
            if cond[0] == 'event':
                horizon = int(cond[1])
                events = data > 0
                past_e = events.rolling(horizon).sum()
                futur_e = events[::-1].rolling(horizon).sum()[::-1]
                events = (past_e + futur_e) > 0
                c_check = events.any(axis=1)

    in_m = pd.Series(False, index=data.index)
    if len(in_map) > 0:
        in_m.iloc[in_map] = True
    else:
        in_m.iloc[:] = True
    in_m = c_check & in_m
    return np.arange(in_m.shape[0])[in_m]


def get_geo_neighbors(id_lat_lon, geo_frame, dist, lat_field, lon_field, k=None):
    dist_ser = np.sqrt(
        (geo_frame[lat_field] - id_lat_lon[0]) ** 2
        + (geo_frame[lon_field] - id_lat_lon[1]) ** 2
    )
    neighbors = dist_ser < dist
    neighbors = neighbors[neighbors].index
    dists = dist_ser[neighbors].values

    if k is not None:
        nearest = dist_ser[neighbors].argsort()[:k]
        neighbors = neighbors[nearest]
        dists = dist_ser[neighbors].values

    return neighbors, dists


def get_dist_mat(meas_frame, metric):
    dist_mat = scipy.spatial.distance.squareform(
        scipy.spatial.distance.pdist(meas_frame.values.T, metric)
    )
    return pd.DataFrame(dist_mat, columns=meas_frame.columns, index=meas_frame.columns)


def get_dist_neighbors(dist_frame, id, thresh=None, k=None):
    if thresh:
        neighbors = dist_frame[id] < thresh
        neighbors = neighbors[neighbors].index
    else:
        neighbors = dist_frame.index
    if k:
        nearest = dist_frame.loc[id, neighbors].values.argsort()[:k]
        neighbors = neighbors[nearest]
    return neighbors


def get_full_ts(meas_frame, ids=None):
    if ids is not None:
        meas_frame = meas_frame.copy()
        meas_frame = meas_frame.loc[:, ids]
    nona = meas_frame.isna().any(axis=1) == False
    return nona[nona].index


def corrupt(meas_frame, c_func, c_val):
    """
    :param c_func:  Function, determining which values to mark as missing. be a function of pandas timeseries and return
                    boolean array.
    :return:
    """
    to_corrupt = pd.DataFrame(
        data=False, columns=meas_frame.columns, index=meas_frame.index
    )
    vals = np.apply_along_axis(lambda row: c_func(row), 1, to_corrupt.values)
    to_corrupt.loc[:, :] = vals
    corrupted = meas_frame.copy()
    corrupted[to_corrupt] = c_val

    return meas_frame, corrupted, to_corrupt


# def get_hinted_data()


def random_drop(x, drop_dist):
    drop_frac = np.random.choice(drop_dist[0], p=drop_dist[1])
    drop_num = np.round(drop_frac * len(x))
    try:
        drop_i = np.random.choice(len(x), int(drop_num), replace=False)
    except ValueError:
        drop_i = np.random.choice(len(x), int(drop_num) - 1, replace=False)
    out_arr = np.array([False] * len(x))
    out_arr[drop_i] = True
    return out_arr


def get_miss_dist(meas_frame):
    na_per_ts = (meas_frame.isna().sum(axis=1)).values
    hist = np.histogram(na_per_ts, bins=np.arange(max(na_per_ts) + 1))
    return hist[1][:-1] / len(meas_frame.columns), hist[0] / sum(hist[0])


def calcRegressionWeights(raster_size, data, sub_index, mode='balanced'):
    labels = pd.cut(data, raster_size, labels=False).replace({np.nan: -1}).astype(int)
    dat_weights = sklearn.utils.class_weight.compute_sample_weight(mode, labels.values, indices=sub_index)
    return dat_weights


def transformer(trafo_dict, to_scale, mode="fwd"):
    # backwards compatibility if clause... ugly:
    if isinstance(trafo_dict, pd.Series):
        # old:
        md = {'fwd': 'div', 'bwd': 'mul'}
        to_scale = getattr(to_scale, md[mode])(trafo_dict[to_scale.columns])
    else:
        # new:
        md = {'fwd': 0, 'bwd': 1}
        for v in to_scale.columns:
            to_scale[v] = trafo_dict[v][md[mode]](to_scale[v])
    if mode not in {'fwd', 'bwd'}:
        raise ValueError(f'Unknown mode key word ({mode}) passed.')
    return to_scale


def parseDWDcsv(
    f_path, id_field="STATIONS_ID", stamp_field="MESS_DATUM", field="NIEDERSCHLAGSHOEHE"
):

    data = pd.read_csv(f_path, usecols=[id_field, stamp_field, field])
    ids = data[id_field].unique()
    m_frame = pd.DataFrame([])
    station = 0
    for id in ids:
        print(f_path)
        print(station)
        id_frame = data[data[id_field] == id]
        id_ser = id_frame.pivot_table(index=stamp_field, columns=id_field, values=field)
        if id_ser.empty:
            m_frame[id] = pd.Series(np.nan, index=pd.DatetimeIndex(id_ser.index))
        else:
            m_frame[id] = pd.Series(
                id_ser.values[:, 0], index=pd.DatetimeIndex(id_ser.index)
            )
        station += 1
    return m_frame


def convert2parquet(folder_path, stamp_field="NIEDERSCHLAGSHOHE", pref=""):
    files = os.listdir(folder_path)
    for f in [fyles for fyles in files if fyles[-3:] == "csv"]:
        m_frame = parseDWDcsv(os.path.join(folder_path, f), field=stamp_field)
        m_frame.columns = [str(x) for x in m_frame.columns]
        m_frame.to_parquet(os.path.join(folder_path, pref + f[:-3] + "parquet"))


def makeGeoFrame(
    folder_path,
    id_field="STATIONS_ID",
    lat_field="GEOGR_BREITE",
    lon_field="GEOGR_LAENGE",
):
    files = os.listdir(folder_path)
    geo_frame = pd.DataFrame([])
    loadF = tqdm([fyles for fyles in files if fyles[-3:] == "csv"])
    for f in loadF:
        m_frame = pd.read_csv(
            os.path.join(folder_path, f), usecols=[id_field, lat_field, lon_field]
        )
        m_frame = m_frame.set_index(id_field)
        m_frame = m_frame.drop_duplicates()
        app_index = m_frame.index.difference(geo_frame.index)
        geo_frame = geo_frame.append(m_frame.loc[app_index, :])

    drop_i = geo_frame.index[geo_frame.index.duplicated()]
    geo_frame = geo_frame.drop(drop_i)
    geo_frame.to_parquet(os.path.join(folder_path, "geoFrame.parquet"))


def deg2cart(datapath='/home/luenensc/PyPojects/NeuraLab/DWDData', folderpath='/home/luenensc/PyPojects/NeuraLab/DWDData/WR'):
    degfiles = os.listdir(folderpath)
    sinfolderpath = os.path.join(datapath, 'Wsin')
    cosfolderpath = os.path.join(datapath, 'Wcos')
    if not os.path.exists(sinfolderpath):
        os.mkdir(sinfolderpath)
    if not os.path.exists(cosfolderpath):
        os.mkdir(cosfolderpath)
    for f in tqdm(degfiles):
        p = os.path.join(folderpath, f)
        deg_data = pd.read_parquet(p)
        rad_data = np.deg2rad(deg_data)
        x = np.cos(rad_data)
        y = np.sin(rad_data)
        x.to_parquet(os.path.join(cosfolderpath, f))
        y.to_parquet(os.path.join(sinfolderpath, f))
    pass


def scaleCart(datapath='/home/luenensc/PyPojects/NeuraLab/DWDData'):
    # WIP WIP WIP
    scalefiles = os.listdir(os.path.join(datapath, 'WS'))
    scaled_sin_folderpath = os.path.join(datapath, 'WsinS')
    scaled_cos_folderpath = os.path.join(datapath, 'WcosS')
    if not os.path.exists(scaled_sin_folderpath):
        os.mkdir(scaled_sin_folderpath)
    if not os.path.exists(scaled_cos_folderpath):
        os.mkdir(scaled_cos_folderpath)
    L = [v for v in scalefiles if v != 'geoFrame.parquet']
    for f in tqdm(L):
        scale_p = os.path.join(datapath, 'WS', f)
        sin_p = os.path.join(datapath, 'Wsin', f)
        cos_p = os.path.join(datapath, 'Wcos', f)
        scale_data = pd.read_parquet(scale_p)
        sin_data = pd.read_parquet(sin_p)
        cos_data = pd.read_parquet(cos_p)
        scaled_sin = scale_data*sin_data
        scaled_cos = scale_data*cos_data
        scaled_sin.to_parquet(os.path.join(scaled_sin_folderpath, f))
        scaled_cos.to_parquet(os.path.join(scaled_cos_folderpath, f))
    pass


def windForceSCP(datapath='/home/luenensc/PyPojects/NeuraLab/DWDData', target_station='5600', wind_ref='15122'):
    cosfiles = os.listdir(os.path.join(datapath, 'WsinS'))
    geoFrame = pd.read_parquet(os.path.join(datapath, 'RR', 'geoFrame.parquet'))
    vecFrame = pd.DataFrame(geoFrame - geoFrame.loc[int(target_station)], index=geoFrame.index)
    trg_folder = os.path.join(datapath, f'{target_station}scpF')
    if not os.path.exists(trg_folder):
        os.mkdir(trg_folder)
    L = [v for v in cosfiles if v != 'geoFrame.parquet']
    for f in tqdm(L):
        sin_p = os.path.join(datapath, 'WsinS', f)
        cos_p = os.path.join(datapath, 'WcosS', f)
        sin_data = pd.read_parquet(sin_p)
        cos_data = pd.read_parquet(cos_p)
        if wind_ref in sin_data.columns:
            out=pd.DataFrame(index=sin_data.index)
            for b in tqdm(vecFrame['GEOGR_BREITE'].index):
                out.loc[:,b] = vecFrame['GEOGR_BREITE'][b]*sin_data[wind_ref] + \
                               vecFrame['GEOGR_LAENGE'][b]*cos_data[wind_ref]

        else:
            out=pd.DataFrame(data=np.nan, index=sin_data.index, columns=vecFrame['GEOGR_BREITE'].index)

        out.columns = [str(x) for x in out.columns]
        out.to_parquet(os.path.join(trg_folder, f))
    pass


def getValidCount(folderpath='/home/luenensc/PyPojects/NeuraLab/DWDData/WR',
                  years=[f'20{x}' for x in range(10, 21)]):
    geo_frame = pd.read_parquet(os.path.join(folderpath, 'geoFrame.parquet'))
    counts = pd.Series(data=0,index=geo_frame.index)
    loadYears = tqdm(years)
    for Y in loadYears:
        loadYears.set_description(f'counting {os.path.basename(folderpath)} in {Y}')
        sensor = pd.read_parquet(os.path.join(folderpath, Y + '.parquet'))
        valids = sensor.notna().sum(axis=0)
        valids.index = valids.index.astype(np.int64)
        counts[valids.index] += valids

    return counts


def makeTrafo(data_ser, tüp):
    # tüp : MinMax, none, logNorm, zNorm
    func = tüp.split('_')[0]
    f_args = tüp.split('_')[1:]
    f_args = {p: float(val) for (p, val) in [f_arg.split(':') for f_arg in f_args]}
    if func == 'MinMax':
        # check for quantile option:
        qmaxx = f_args.pop('qmaxx', None)
        if qmaxx:
            f_args.update({'maxx': data_ser.quantile(qmaxx)})

        in_args = {'minn': data_ser.min(), 'maxx': data_ser.max()}
        in_args.update(f_args)
        trafo = partial(MinMax, mode='fwd', **in_args)
        inv_trafo = partial(MinMax, mode='bwd', **in_args)

    if func == 'zNorm':
        in_args = {'mean': data_ser.mean(), 'std': data_ser.std()}
        in_args.update(f_args)
        trafo = partial(zNorm, mode='fwd', **in_args)
        inv_trafo = partial(zNorm, mode='bwd', **in_args)

    if func == 'none':
        trafo = inv_trafo = identity

    if func == 'logNorm':
        # defaultly fit the base value shift to minimize distribution skewness:
        opt_skew = f_args.pop('opt_skew', True)
        if opt_skew:
            f_obj = lambda x: np.abs(scipy.stats.skew(np.log(.1 ** x + data_ser)))
            skew_min = scipy.optimize.minimize(f_obj, x0=2, bounds=[(.5, 15)]).x[0]
        else:
            skew_min = 2
        temp = np.log(.1**skew_min + data_ser)
        log_mean = temp.mean()
        log_std = temp.std()
        trafo = partial(logNorm, add_exp=skew_min, log_mean=log_mean, log_std=log_std, mode='fwd')
        inv_trafo = partial(logNorm, add_exp=skew_min, log_mean=log_mean, log_std=log_std, mode='bwd')

    return trafo, inv_trafo


def zNorm(x, mean=0, std=1, mode='fwd'):
    if mode == 'fwd':
        if std == 0:
            std = 1
        return (x - mean)/std
    if mode == 'bwd':
        return (x*std) + mean

def identity(x):
    return x


def MinMax(x, minn=0, maxx=1, mode='fwd'):
    if mode=='fwd':
        if minn == maxx:
            return x
        else:
            return (x - minn) / (maxx - minn)
    if mode=='bwd':
        return (x * (maxx - minn)) + minn


def logNorm(x, add_exp=1, log_mean=0, log_std=1, mode='fwd'):
    if mode == 'fwd':
        return zNorm(np.log(.1**add_exp + x), mean=log_mean, std=log_std, mode=mode)
    if mode == 'bwd':
        return np.exp(zNorm(x, mean=log_mean, std=log_std, mode=mode)) - .1**add_exp


def randomizer(subset_frame, var):
    i_mask = np.arange(0, len(subset_frame.loc[:, var]))[subset_frame.loc[:, var].notna().values]
    np.random.shuffle(i_mask)
    subset_frame[var] = subset_frame[var].iloc[i_mask]
    return subset_frame


def warpPredictor(source, target, agg_func='max', dtw_map=imsdtw.IMSDTW()):
    dist, map = dtw_map(target, source)
    map_frame = pd.DataFrame(map, columns=['target', 'source'])
    map_frame['source'] = source.iloc[map_frame['source']].values
    warped = getattr(map_frame.groupby('target'), agg_func)().squeeze().values
    return warped, map


def rollingWarp(source, target, fill, winsz, dtw_map=imsdtw.IMSDTW(), aggfunc='max'):
    # window based warp, assuming len(source)=len(target) and source,target
    # regularly sampled
    # winsz must be odd
    dilation = np.array([np.nan]*len(source))
    glob_map = np.array([np.nan]*len(source))
    warped = np.array([np.nan]*len(source))
    mid_i = int(.5*(winsz - 1))
    glob_map[:mid_i] = np.arange(mid_i)
    glob_map[-mid_i:] = np.arange(mid_i) + len(glob_map) - mid_i
    dilation[:mid_i] = 0
    dilation[-mid_i:] = 0
    for k in tqdm(np.arange(mid_i, len(target)-mid_i)):
        _mit_i = mid_i
        trg = target[k-_mit_i:k+_mit_i+1]
        src = source[k-_mit_i:k+_mit_i+1]
        trg[_mit_i] = fill[k]
        dist, map = dtw_map(trg, src)
        if np.isnan(dist):
            src_mask = np.isnan(src).values
            trg_mask = np.isnan(trg).values
            src_na_i = np.nonzero(src_mask)[0]
            trg_na_i = np.nonzero(trg_mask)[0]
            na_i = np.concatenate([src_na_i, trg_na_i])
            na_i.sort()
            w = na_i - _mit_i
            w_bef = w[w < 0]
            w_aft = w[w > 0]
            if len(w_bef)>0:
                w_start = w_bef[-1] + _mit_i + 1
            else:
                w_start = 0
            if len(w_aft>0):
                w_end = w_aft[0] + _mit_i
            else:
                w_end = None

            src = src.iloc[w_start:w_end]
            trg = trg.iloc[w_start:w_end]
            _mit_i = mid_i - w_start
            dist, map = dtw_map(trg, src)
        map = np.array(map)
        mapped = map[:, 1] == _mit_i
        map_i = np.nonzero(mapped)[0][0]
        m_i = map[map_i ,0]
        glob_map[k] = m_i - _mit_i + k
        dilation[k] = m_i - _mit_i

    #glob_map = glob_map.astype(int)

    projection = getattr(pd.DataFrame({'target_i': glob_map, 'val': source}).groupby('target_i'),aggfunc)()
    projection = projection[projection.index.notna()]
    projection.index = projection.index.astype(int)
    warped[projection.index] = projection.squeeze().values
    warped = pd.Series(warped).interpolate('linear').values
    dilation = pd.Series(dilation).interpolate('linear').values
    return warped, dilation

if __name__ == '__main__':
    windForceSCP(datapath='/home/luenensc/PyPojects/NeuraLab/DWDData', target_station='5600', wind_ref='15122')
    makeGeoFrame(folder_path='/home/luenensc/PyPojects/NeuraLab/DWDData/5600scpF')



