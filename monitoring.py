from preprocessing import *

# from keras import backend as K
import pandas as pd
import numpy as np
import tensorflow as tf

# increase pandas rows that are displayed
pd.set_option("display.max_rows", 500)


def r2(y_true, y_pred):
    SS_res = tf.keras.backend.sum(tf.keras.backend.square(y_true - y_pred))
    SS_tot = tf.keras.backend.sum(
        tf.keras.backend.square(y_true - tf.keras.backend.mean(y_true))
    )
    return 1 - SS_res / (SS_tot + tf.keras.backend.epsilon())


def ccc(y_true, y_pred):
    """Lin's Concordance correlation coefficient in keras
    https://en.wikipedia.org/wiki/Concordance_correlation_coefficient"""

    if tf.keras.backend.int_shape(y_pred)[-1] is not None:
        # in case y_pred contains uncertainty axis,
        # only use the mean prediction
        y_pred = y_pred[..., 0]
        y_true = y_true[..., 0]

    # means
    x_m = tf.keras.backend.mean(y_true)
    y_m = tf.keras.backend.mean(y_pred)
    # variances
    s_x_sq = tf.keras.backend.var(y_true)
    s_y_sq = tf.keras.backend.var(y_pred)

    # covariance between y_true and y_pred
    s_xy = tf.keras.backend.mean(y_true * y_pred, axis=-1) - x_m * y_m
    # print('cov keras: ', K.eval(s_xy))

    return (2.0 * s_xy) / (s_x_sq + s_y_sq + (x_m - y_m) ** 2)


def ccc_numpy(y_true, y_pred):
    """Lin's Concordance correlation coefficient in numpy
    https://en.wikipedia.org/wiki/Concordance_correlation_coefficient"""
    import numpy as np

    y_true = np.squeeze(y_true)
    y_pred = np.squeeze(y_pred)
    if y_pred.ndim > y_true.ndim:
        print("...")
        y_pred = y_pred[..., 0]

    def ccc(x, y):

        s_xy = np.cov([x, y], bias=True)[0, 1]

        # means
        x_m = np.mean(x)
        y_m = np.mean(y)
        # variances
        s_x_sq = np.var(x)
        s_y_sq = np.var(y)

        # condordance correlation coefficient
        ccc = (2.0 * s_xy) / (s_x_sq + s_y_sq + (x_m - y_m) ** 2)
        return ccc

    return ccc(y_true, y_pred)


def LRscheduler(epoch, lr):
    if epoch < 50:
        return lr
    else:
        return lr*.93


