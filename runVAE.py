from __future__ import annotations

from typing import Tuple, Union
from typing_extensions import Literal
import matplotlib.pyplot as plt
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from monitoring import r2, ccc_numpy, ccc, LRscheduler
from preprocessing import *
from modelConstrunction import *
import os
import random
import pickle
from analytics import Analyze
from preprocessing import TSGen, get_index_map
from tqdm import tqdm
import dill
import re
from sir_tools import prepare_data
import pickle5 as pickle

class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon

class VAE(keras.Model):
    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
        ]

    def train_step(self, data):
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.mean_squared_error(data, reconstruction), axis=(0)
                )
            )
            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            total_loss = reconstruction_loss + kl_loss
        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
        }

    def call(self, inputs):
        z_mean, z_log_var, z = encoder(inputs)
        reconstruction = decoder(z)

        return reconstruction


def return_center_window(date: int, var_trg: int, d_shape: Tuple[int, int], sub_len: int):
    """
    :param center: date
    :param var_trg: variable number in in frame
    :param var_N: variable count
    :param sub_len: window size
    :return: data_row
    """
    left_start = date - (np.floor(sub_len / 2))
    return int(left_start*d_shape[1] + var_trg), sub_len // 2

def return_all_covering_windows(date:int, var_trg: int, d_shape: Tuple[int, int], sub_len: int):
    """
    :param center: date
    :param var_trg: variable number in in frame
    :param var_N: variable count
    :param sub_len: window size
    :param return_center_i: index of the centered value
    :return: data_row
    """
    r0 = max(int(date - np.floor(sub_len/2)), sub_len//2)
    r1 = min(int(date + np.floor(sub_len/2) + 1), d_shape[0] - (sub_len//2))
    i_arr = np.zeros(r1-r0, dtype=int)
    c_arr = np.zeros(r1-r0, dtype=int)
    for c in enumerate(range(r0,r1)):
        i_arr[c[0]], _ = return_center_window(c[1], var_trg, d_shape, sub_len)
        c_arr[c[0]] = sub_len//2 - (c[1] - date)
    return i_arr, c_arr


def reconstruct_variable(var_trg:int, d_shape:Tuple[int, int], sub_len:int, rc_range:Tuple[int, int], data, func=None):
    rc_var = np.zeros(rc_range[1] - rc_range[0])
    if not func:
        for d in enumerate(range(rc_range[0],rc_range[1])):
            d_ind, c_ind = return_center_window(d[1], var_trg, d_shape, sub_len)
            rc_var[d[0]] = data[d_ind, c_ind]
    else:
        for d in enumerate(range(rc_range[0],rc_range[1])):
            d_ind, c_ind = return_all_covering_windows(d[1], var_trg, d_shape, sub_len)
            rc_var[d[0]] = func(data[d_ind, c_ind])

    return rc_var


if __name__ == "__main__":

    ####################################################################################################################
    # PARSING
    ####################################################################################################################
    sub_len = 335 - 100 # only odd values supported
    start_date = '2020-01-01'
    end_date = '2020-12-01'
    filename = "/home/luenensc/PyPojects/covid_outlier/inputs/W2T_RKI_COVID19.pkl"
    with open(filename, 'rb') as f:
        df = pickle.load(f)

    df.index = pd.DatetimeIndex(df["date"])
    #df = df[df["name"] == kreis_name]

    df["date"] = pd.to_datetime(df["date"])
    df = df[["infected","name"]]
    df = df.pivot(columns='name', values='infected')
    df = df[start_date:end_date]
    df = df.resample("D").asfreq()
    df = df.fillna(0)
    df = df/df.max().max()

    fix_seed = 100
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)



    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################
    i_map = np.arange(0,len(df.columns))
    train_percentage = 1
    test_percentage = 0
    perm_train = i_map

    # make VAE

    latent_dim = 3

    encoder_inputs = keras.Input(shape=(sub_len))
    x = layers.Dense(sub_len//4, activation="relu")(encoder_inputs)
    x = layers.Dense(sub_len//8, activation="relu")(x)
    z_mean = layers.Dense(latent_dim, name="z_mean")(x)
    z_log_var = layers.Dense(latent_dim, name="z_log_var")(x)
    z = Sampling()([z_mean, z_log_var])
    encoder = keras.Model(encoder_inputs, [z_mean, z_log_var, z], name="encoder")
    encoder.summary()

    latent_inputs = keras.Input(shape=(latent_dim,))
    x = layers.Dense(sub_len // 8, activation="relu")(latent_inputs)
    x = layers.Dense(sub_len // 4, activation="relu")(x)
    decoder_outputs = layers.Dense(sub_len, activation="relu")(x)
    decoder = keras.Model(latent_inputs, decoder_outputs, name="decoder")
    decoder.summary()


x_train = df.values[:, perm_train]
# generate recovery map
map = np.empty(x_train.shape, dtype=object)
for k in range(0, x_train.shape[0]):
    for j in range(0, x_train.shape[1]):
        map[k, j] = (k,j)

# split input:
split = np.lib.stride_tricks.sliding_window_view(x_train, sub_len, axis=0)
x_train = split.reshape(split.shape[0]*split.shape[1], split.shape[2])

map_split = np.lib.stride_tricks.sliding_window_view(map, sub_len, axis=0)
map = map_split.reshape(map_split.shape[0]*map_split.shape[1], map_split.shape[2])

rc=reconstruct_variable(var_trg=130, d_shape=df.shape, sub_len=sub_len, rc_range=(0, df.shape[0]), data=x_train, func=np.mean)

model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath='/home/luenensc/PyPojects/NeuraLab/BestWeights/currentRun',
    save_weights_only=True,
    monitor='loss',
    mode='min',
    save_best_only=True)

vae = VAE(encoder, decoder)
vae.compile(optimizer=keras.optimizers.Adam())
vae.fit(x=(x_train), epochs=100, batch_size=10, callbacks=[model_checkpoint_callback])
vae.load_weights('/home/luenensc/PyPojects/NeuraLab/BestWeights/currentRun')
y = vae.predict(x_train)

rc_func=np.mean
for k in [130]:
    fig, axes = plt.subplots(5, 2, sharey=True)
    for p in range(0, 5):
        district = k + p
        axes[p, 0].plot(reconstruct_variable(var_trg=district, var_N=df.shape[1], sub_len=sub_len, rc_range=(0, df.shape[0]), data=x_train, func=rc_func))
        axes[p, 0].plot(y[district, :])
        axes[p, 0].set_title(df.columns[district])
        axes[p, 1].plot(np.abs(x_train[district, :] - y[district, :]))
        axes[p, 1].set_title(df.columns[district])

#print('stop')