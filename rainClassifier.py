import logging
import os
import pandas as pd
import numpy as np
import os

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from xgboost import XGBClassifier
from preprocessing import TSGen

from preprocessing import get_geo_neighbors

logging.basicConfig(format='[%(asctime)s]: %(levelname)s: %(message)s', level=logging.INFO)


class NotEnoughData(Exception):
    pass


class RainClassifier:

    def __init__(self, station_dist, scale_pos_weight):
        self.station_dist = station_dist
        self.scale_pos_weight = scale_pos_weight
        self.years = ["2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"]
        self.geo_frame = pd.read_parquet(os.path.join("DWDData/" + "RR", "geoFrame.parquet"))

    def fetch_dwd_data(self, station_id):
        """Fetches precipitation, humidity and sunshine data for given station_id and all geo_neighbors!"""
        geo_dict = self.geo_frame.to_dict(orient="index")
        station_id_lat_lon = (geo_dict[int(station_id)]["GEOGR_BREITE"], geo_dict[int(station_id)]["GEOGR_LAENGE"])
        neighbors, dist = get_geo_neighbors(
            station_id_lat_lon, self.geo_frame, self.station_dist, "GEOGR_BREITE", "GEOGR_LAENGE")
        station_ids_str = [str(i) for i in neighbors]
        df_rain_all_years = pd.DataFrame()
        df_rh_all_years = pd.DataFrame(columns=[station_id])
        df_sd_all_years = pd.DataFrame(columns=[station_id])
        for year in self.years:
            df = pd.read_parquet('DWDData/RR/{0}.parquet'.format(year))
            df = df[[station for station in station_ids_str if station in list(df.columns)]]
            df_rain_all_years = pd.concat([df_rain_all_years, df], ignore_index=False)
            df = pd.read_parquet('DWDData/rH/{0}.parquet'.format(year))
            df_rh_all_years = pd.concat([df_rh_all_years, df], ignore_index=False)
            df = pd.read_parquet('DWDData/SD/{0}.parquet'.format(year))
            df_sd_all_years = pd.concat([df_sd_all_years, df], ignore_index=False)

        number_of_data = len(df_rain_all_years[station_id])
        if df_rain_all_years[station_id].count() / number_of_data >= 0.95:
            logging.info("DWD data fetched for station {0}".format(station_id))
            return df_rain_all_years, df_rh_all_years[station_id], df_sd_all_years[station_id]
        else:
            raise NotEnoughData

    def join_parameter_data(self, station_id):
        """Joins precipitation, humidity and sunshine data on datetime index!"""
        precipitation, humidity, sunshine = self.fetch_dwd_data(station_id)
        df_rain_rh = precipitation.join(humidity, how="outer", lsuffix="", rsuffix="rh")
        df_joined = df_rain_rh.join(sunshine, how="outer", lsuffix="", rsuffix="sd")
        logging.info("Parameter data joined for station {0}".format(station_id))

        return df_joined

    def aggregate_hourly_values(self, station_id):
        """Aggregates 10 minute values to hourly values"""
        df_joined = self.join_parameter_data(station_id)
        df_hourly = df_joined.resample("1h").sum(min_count=6)
        if "{0}rh".format(station_id) in df_hourly.columns:
            df_hourly["{0}rh".format(station_id)] = df_hourly["{0}rh".format(station_id)] / 6
        logging.info("Hourly data aggregated for station {0}".format(station_id))

        return df_hourly

    def remove_bad_columns(self, station_id, threshold=0.95):
        """Removes all columns/features with more than 5% missing data!"""
        df_hourly = self.aggregate_hourly_values(station_id)
        df_hourly = df_hourly.dropna(axis=1, thresh=threshold * len(df_hourly))

        return df_hourly

    def prepare_data(self, station_id):
        """Categorizes rain and adds features!"""
        df_hourly = self.remove_bad_columns(station_id)
        df_hourly['did_rain'] = np.where(df_hourly[station_id] > 0, 1, 0)
        df_hourly["plus_one_hour"] = df_hourly[station_id].shift(periods=-1)
        df_hourly["minus_one_hour"] = df_hourly[station_id].shift(periods=1)
        for column in df_hourly.columns:
            if not any(ext in column for ext in ["rh", "sd", "did_rain", "hour"]):
                df_hourly["{}_plus_one_hour".format(column)] = df_hourly[str(column)].shift(periods=-1)
                df_hourly["{}_minus_one_hour".format(column)] = df_hourly[str(column)].shift(periods=1)

        df_model = df_hourly.loc[:"2019"]
        df_predict = df_hourly.loc["2020":]

        return df_model, df_predict

    @staticmethod
    def get_features(station_id, df_model):
        """Extracts features from df columns!"""
        non_features = [station_id, "did_rain", "{}_minus_one_hour".format(station_id),
                        "{0}_plus_one_hour".format(station_id)]
        features = [item for item in df_model.columns if item not in non_features]

        return features

    @staticmethod
    def create_train_and_test_data(df_model, features):
        x = df_model[features]
        y = df_model["did_rain"]

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42, stratify=y)

        return x_train, x_test, y_train, y_test

    def train_classifier(self, df_model, features):
        x_train, x_test, y_train, y_test = self.create_train_and_test_data(df_model, features)

        model = XGBClassifier(objective="binary:logistic", learning_rate=0.1, max_depth=3, n_estimators=200,
                              use_label_encoder=False, scale_pos_weight=self.scale_pos_weight, eval_metric="error")
        clf = model.fit(x_train, y_train)
        logging.info(classification_report(y_test, clf.predict(x_test)))

        return clf

    def predict(self, station_id, df_model, df_predict, features):
        """Uses trained classifier to predict latest data!"""
        clf = self.train_classifier(df_model, features)
        prediction = clf.predict(df_predict[features])
        df_prediction = pd.DataFrame(list(prediction), list(df_predict.index), columns=[station_id])
        df_prediction_hourly = df_prediction.resample("10min").pad()

        return df_prediction_hourly

    def get_station_prediction_df(self, station_id):
        """Returns prediction DataFrame for given stationID!"""
        df_model, df_predict = self.prepare_data(station_id)
        features = self.get_features(station_id, df_model)
        prediction = self.predict(station_id, df_model, df_predict, features)

        return prediction

    def create_prediction_df(self, stations_list):
        """Returns prediction DataFrame for a list of stations!"""
        df_result = pd.DataFrame()
        for station_id in stations_list:
            try:
                prediction = self.get_station_prediction_df(station_id)
                df_result = pd.merge(prediction, df_result, left_index=True, right_index=True, how="outer")
                logging.info("Added prediction for station {0}!".format(station_id))
            except NotEnoughData:
                logging.warning("Station {0} has not enough precipitation data for classification!".format(station_id))
                pass

        return df_result

    def prediction_to_parquet(self, stations_list):
        """Transforms prediction DataFrame toh parquet file!"""
        df_prediction = self.create_prediction_df(stations_list)
        if not os.path.exists('Results/classification'):
            os.mkdir('Results/classification')
        df_prediction.to_parquet(
            "Results/classification/dist={0}_weight={1}_prediction.parquet".format(self.station_dist, self.scale_pos_weight))


def main():
    stations_list = ["5600"]
    c = RainClassifier(station_dist=0.4, scale_pos_weight=10)
    c.prediction_to_parquet(stations_list)



main()
