import tensorflow as tf
from sklearn.model_selection import train_test_split
from monitoring import r2, ccc_numpy, ccc, LRscheduler
from preprocessing import *
from modelConstrunction import *
import os
import random
import pickle
from analytics import Analyze
from preprocessing import TSGen, get_index_map
from tqdm import tqdm
import dill
import re


if __name__ == "__main__":

    ####################################################################################################################
    # PARSING
    ####################################################################################################################
    #input('storing Val_I for Test_I')
    #input('WARPING!')
    fix_seed = 100
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)

    Exp_folder = "Results/currentRun"
    run_name = "MonteCarloDO"
    #warp_on = "warbBenchUnwarpedRef"
    #warp_on = (os.path.join('currentRun', warp_on), 1)
    log_dir = os.path.join(Exp_folder, run_name, "logs")
    c_id = 5600
    center_radius = .5
    k_neighbors = 4
    min_valids = 100000
    norm_dict = {'default': 'MinMax',
                 'RR': 'MinMax_qmaxx:0.99'}
    sub_len = 6
    s_rate = '10min'
    id_field = "STATIONS_ID"
    lat_field = "GEOGR_BREITE"
    lon_field = "GEOGR_LAENGE"
    stamp_field = "MESS_DATUM"
    field = "NIEDERSCHLAGSHOEHE"
    years = [
        "2010",
        "2011",
        "2012",
        "2013",
        "2014",
        "2015",
        "2016",
        "2017",
        "2018",
        "2019",
        #"2020"
    ]
    target_var = f'{c_id}_RR'
    warp_fill = 'linear'
    generator_ini_dict = {
        "sub_len": sub_len,
        "flatten": True,
        "pred_mode": "embedded",
        "pred_id": target_var,
        "miss_val": 0,
        "center_target": True,
        "rm_target_from_flat": True,
        "warp": False,
        "warp_fill": warp_fill,
        "warp_range": 2
         }


    # retrieve center ID coordinates:
    geo_frame = pd.read_parquet(os.path.join("DWDData/" + "RR", "geoFrame.parquet"))
    c_lat = geo_frame.loc[c_id,:][0]
    c_lon = geo_frame.loc[c_id,:][1]
    subset_frame = pd.DataFrame([])
    allIDS = []
    disdict = {}
    var_load = tqdm(['RR'])#, '5600scpF'])
    for var in var_load:
        var_load.set_description(f"Loading {var}")
        folder_path = "DWDData/" + var
        file_folder = var
        geo_frame = pd.read_parquet(os.path.join(folder_path, "geoFrame.parquet"))
        count = getValidCount(folder_path, years=years)
        vStations = count[count < min_valids].index

        # warn if target station is heavily underpopulated
        if (c_id in vStations) and (var=='RR'):
            print(f'Target Station (={c_id}), has less then the minimum number of valid values (={min_valids})!')
            vStations=vStations.drop(c_id)
        # drop selfSCP for centerID
        if (re.sub('[0-9]','', var)=='scpF'):
            vStations = vStations.append(pd.Index([int(re.sub('[^0-9]','', var))], dtype=np.int64))
        geo_frame.drop(vStations, 0, inplace=True)
        IDs, dists = get_geo_neighbors((c_lat, c_lon), geo_frame, center_radius, lat_field, lon_field, k=k_neighbors)
        allIDS.append(IDs)
        disdict.update(dict(zip(IDs, dists)))
        A = Analyze(run="dummy1/dummy2", RR_folder=file_folder)
        A.load_data(years, load_mode=[str(x) + "_" + var for x in IDs])
        subset_frame[A.X.columns] = A.X.loc[~A.X.index.duplicated(keep="first"), :]
        # filter out distorting Stations
    subset_frame = subset_frame.resample(s_rate).asfreq()
    # filter for complete records
    i_map = get_index_map(subset_frame, sub_len + 1, s_rate="10min")
    i_map = get_index_map(subset_frame, - sub_len - 1, s_rate="10min", in_map=i_map)

    i_map = get_index_map(
        subset_frame, f"wet@5600", s_rate="10min", in_map=i_map
    )



    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################

    #perm_val = subset_frame.index[i_map].year >= 2020
    #perm_val = i_map[perm_val]
    #perm_train = subset_frame.index[i_map].year < 2020
    #perm_train = i_map[perm_train]
    #np.random.shuffle(perm_train)
    #perm_test = np.array([], dtype=int)
    #wet_i = pd.read_csv('/home/luenensc/PyPojects/NeuraLab/Results/classification/rain_i.csv')
    #wet_i = pd.DatetimeIndex(wet_i.iloc[:, 0].values)
    #perm_val = np.array([subset_frame.index.get_loc(wet_i[i]) for i in range(len(wet_i))])
    #perm_val = np.array([a for a in perm_val if a in i_map])
    train_percentage = 0.8
    test_percentage = 0
    perm_train, perm_test = train_test_split(
        i_map, test_size=(1 - train_percentage), random_state=fix_seed
    )

    if test_percentage > 0:
        perm_test, perm_val = train_test_split(
            perm_test, test_size=(1 - test_percentage), random_state=fix_seed
        )
    else:
        perm_val = perm_test
        perm_test = np.array([], dtype=int)

    # make trafos
    trafo_dict = {}
    trafo_i = subset_frame.index[i_map]
    for v in subset_frame.columns:
        v_tüpp = v.split('_')[-1]
        if v_tüpp in norm_dict.keys():
            trafo_dict.update({v: makeTrafo(subset_frame.loc[trafo_i, v], norm_dict[v_tüpp])})
        else:
            trafo_dict.update({v: makeTrafo(subset_frame.loc[trafo_i, v], norm_dict['default'])})


    trafo_dict.update({'5600_RR': trafo_dict['801_RR']})
    trafo_dict.update({'6283_RR': trafo_dict['801_RR']})
    trafo_dict.update({'3179_RR': trafo_dict['801_RR']})

    subset_frame = transformer(trafo_dict, subset_frame)

    indäx = subset_frame.index
    train_i, val_i, test_i = (indäx[perm_train], indäx[perm_val], indäx[perm_test])

    # load data generators
    generator_ini_dict.update({"x_data": subset_frame.copy(),
                               "y_data": subset_frame[target_var].copy(),
                               "weights": None})

    train_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(perm_train),
        gen_index=perm_train,
    )


    val_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(perm_val),
        gen_index=perm_val,
    )

    # dict for scenario recovery in evaluation phase
    generator_dict = {
        "sub_len": train_generator.sub_len,
        "flatten": train_generator.flatten,
        "pred_mode": train_generator.pred_mode,
        "pred_id": target_var,
        "miss_val": train_generator.miss_val,
        "center_target": train_generator.center_target,
        "rm_target_from_flat": train_generator.rm_target_from_flat,
        "warp": train_generator.warp,
        "warp_fill": warp_fill
    }

    ####################################################################################################################
    # Prepare Experiment Storage / doc Experiment scenario
    ####################################################################################################################

    if not os.path.exists(Exp_folder):
        os.mkdir(Exp_folder)

    ####################################################################################################################
    # Run
    ####################################################################################################################
    Model_Num = 0

    run_path = os.path.join(Exp_folder, run_name)
    if not os.path.exists(run_path):
        os.mkdir(run_path)

    ########################################
    # Generate reconstruction Info for the run:
    ########################################
    info = pd.Series(
        {
            "center_id": c_id,
            "radius": center_radius,
            "seed": fix_seed,
            "years": years,
            "corruption": "none",
            "ts_drop": "any",
            "Data": "RR + rH",
            "Note": "only wet Days",
        }
    )
    with open(os.path.join(Exp_folder, run_name, 'trafo.pickle'), 'wb') as f:
        dill.dump(trafo_dict, f)
    generator_config = pd.DataFrame(generator_dict, index=["opt_val"])

    generator_config.to_csv(os.path.join(run_path, "generator_config.csv"))
    info.to_csv(os.path.join(run_path, "info.csv"))
    pd.Series(allIDS).to_csv(os.path.join(run_path, "IDs.csv"), index=False)
    train_i.to_series().to_csv(os.path.join(run_path, "train_index.csv"), index=False)
    val_i.to_series().to_csv(
        os.path.join(run_path, "validation_index.csv"), index=False
    )
    #test_i.to_series().to_csv(os.path.join(run_path, "test_index.csv"), index=False)
    test_i.to_series().to_csv(os.path.join(run_path, "test_index.csv"), index=False)
    pd.DataFrame(train_generator.x_data).columns.to_series().to_csv(
        os.path.join(run_path, "IDx.csv"), index=False
    )
    pd.DataFrame(train_generator.y_data).columns.to_series().to_csv(
        os.path.join(run_path, "IDy.csv"), index=False
    )

    n_inputs = (train_generator.x_data.shape[1]) * (
        train_generator.sub_len + train_generator.future_increment
    ) - (train_generator.rm_target_from_flat * 1)
    loss = "mean_squared_error"
    metrics = [ccc, "mae", r2]
    scores = pd.DataFrame([])
    tf.keras.backend.clear_session()
    initializer = tf.keras.initializers.glorot_normal(seed=fix_seed)
    # model = tf.keras.models.Sequential()
    layers = tf.keras.layers

    moddels=[]
    for k in [5]:
        moddels.append(tf.keras.Sequential([
            layers.Flatten(),
            layers.Dense(n_inputs*2),
            layers.Dropout(rate=.5),
            layers.Dense(n_inputs, activation='relu'),
            layers.Dropout(rate=.5),
            layers.Dense(1, activation='relu')
        ]))


    ####################################################################################################################
    # Training Models
    ####################################################################################################################

    for m in moddels:
        # train para[i] = (epochs: int, batch_num: int, shuffle: bool, rate_adapt: bool, rate_schedule:bool, lr:float)
        for train_para in [(200, 2000, False, False, True, 1e-3), (200, 2000, False, True, True, 1e-3),
                           (200, 2000, False, True, True, 1e-4)]:

            n_epochs = train_para[0]
            batch_size = train_para[1]
            train_generator.set_batch_size(batch_size)
            train_generator.shuffle = train_para[2]
            use_lrAdapt = train_para[3]
            use_lrSchedule = train_para[4]

            # callback config
            tensorboardLogs = tf.keras.callbacks.TensorBoard(log_dir=os.path.join(log_dir, str(Model_Num)),
                                                             histogram_freq=1)


            stopper = tf.keras.callbacks.EarlyStopping(
                monitor="val_loss",
                min_delta=0,
                patience=np.ceil(n_epochs * .1),
                verbose=0,
                mode="min",
                baseline=None,
            )

            checker = tf.keras.callbacks.ModelCheckpoint(
                filepath=os.path.join(run_path, str(Model_Num) + "_bestLoss"),
                save_weights_only=True,
                monitor="val_loss",
                mode="min",
                save_best_only=True,
            )

            LRadapter = tf.keras.callbacks.ReduceLROnPlateau(
                monitor="val_loss",
                factor=0.5,
                patience=10,
                verbose=1,
                mode="min",
                cooldown=10,
            )
            LRsched = tf.keras.callbacks.LearningRateScheduler(LRscheduler, verbose=0)

            c_list = [stopper, checker, tensorboardLogs]
            if use_lrAdapt:
                c_list.append(LRadapter)

            if use_lrSchedule:
                c_list.append(LRsched)

            drop_list = []
            for lay in m.layers:
                if lay.name.split("_")[0] == "dropout":
                    drop_list.append(lay.rate)

            m.compile(
                optimizer=tf.keras.optimizers.Adam(lr=train_para[5]),
                loss="mean_squared_error",
                metrics=metrics,
            )
            history = m.fit(
                x=train_generator,
                epochs=n_epochs,
                validation_data=val_generator,
                use_multiprocessing=False,
                callbacks=c_list,
            )

            with open(
                os.path.join(run_path, str(Model_Num) + "_history.pickle"), "wb"
            ) as f:
                pickle.dump(history.history, f)

            with open(
                os.path.join(run_path, str(Model_Num) + "_summary.txt"), "w"
            ) as fh:
                m.summary(print_fn=lambda x: fh.write(x + "\n"))

            m.save(os.path.join(run_path, str(Model_Num) + "_model"))

            loss_min_arg = np.array(history.history["val_loss"]).argmin()

            res = pd.DataFrame(
                {
                    "loss_ratio_final": (history.history["val_loss"][-1])
                    / (history.history["loss"][-1]),
                    "loss_ratio_@loss_val_min": history.history["val_loss"][
                        loss_min_arg
                    ]
                    / history.history["loss"][loss_min_arg],
                    "loss_val_min": min(history.history["val_loss"]),
                    "loss_val_final": history.history["val_loss"][-1],
                    "loss_train_final": history.history["loss"][-1],
                    "r2_val_max": max(history.history["val_r2"]),
                    "r2_val_final": history.history["val_r2"][-1],
                    "r2_train_final": history.history["r2"][-1],
                    "ccc_val_max": max(history.history["val_ccc"]),
                    "ccc_val_final": history.history["val_ccc"][-1],
                    "ccc_train_final": history.history["ccc"][-1],
                    "epochs": len(history.history[list(history.history.keys())[0]]),
                    "batch": batch_size,
                    "shuffle_on_ee": train_generator.shuffle,
                    "LRAdaption": use_lrAdapt,
                    "drops": str([round(x, 1) for x in drop_list]),
                },
                index=[Model_Num],
            )

            scores = scores.append(res)
            scores.to_csv(os.path.join(run_path, "all_scores.csv"))
            res.to_csv(os.path.join(run_path, str(Model_Num) + "_score.csv"))
            Model_Num += 1
    if not scores.empty:
        scores.to_csv(os.path.join(run_path, "all_scores.csv"))



