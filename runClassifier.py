import tensorflow as tf
from sklearn.model_selection import train_test_split
from monitoring import r2, ccc_numpy, ccc, LRscheduler
from preprocessing import *
from modelConstrunction import *
import os
import random
import pickle
from analytics import Analyze
from preprocessing import TSGen, get_index_map
from tqdm import tqdm
import dill
import re
from xgboost import XGBClassifier
import sklearn
from analytics import scoreClassifier, feature_mapper
import itertools
import matplotlib.pyplot as plt

PARSE_DICT = {'id_field': "STATIONS_ID",
              'lat_field': "GEOGR_BREITE",
              'lon_field': "GEOGR_LAENGE",
              'stamp_field': "MESS_DATUM",
              'field':"NIEDERSCHLAGSHOEHE"}

def train_station(c_id,
                  center_radius=5,
                  k_neighbors=3,
                  sub_len=18,
                  min_valids=50000,
                  norm_dict={'default': 'MinMax', 'RR': 'MinMax_qmaxx:0.99'},
                  years=None,
                  testyear='2020',
                  s_rate='10min',
                  fix_seed=None):
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)

    if not years:
        years = ['20'+str(k) for k in range(10,21)]


    target_var = f'{c_id}_RR'
    generator_ini_dict = {
        "sub_len": sub_len,
        "flatten": True,
        "pred_mode": "embedded",
        "pred_id": target_var,
        "miss_val": 0,
        "center_target": True,
        "rm_target_from_flat": True,
        "warp": False,
        "warp_fill": 'linear',
        "warp_range": 2
    }

    # retrieve center ID coordinates:
    geo_frame = pd.read_parquet(os.path.join("DWDData/" + "RR", "geoFrame.parquet"))
    c_lat = geo_frame.loc[c_id, :][0]
    c_lon = geo_frame.loc[c_id, :][1]
    subset_frame = pd.DataFrame([])
    allIDS = []
    disdict = {}
    var_load = tqdm(['RR'])  # , '5600scpF'])
    for var in var_load:
        var_load.set_description(f"Loading {var}")
        folder_path = "DWDData/" + var
        file_folder = var
        geo_frame = pd.read_parquet(os.path.join(folder_path, "geoFrame.parquet"))
        count = getValidCount(folder_path, years=years)
        vStations = count[count < min_valids + 50000].index
        #vStations = vStations.append(pd.Index([801]))
        # warn if target station is heavily underpopulated
        if (c_id in vStations) and (var == 'RR'):
            print(f'Target Station (={c_id}), has less then the minimum number of valid values (={min_valids})!')
            vStations = vStations.drop(c_id)

        geo_frame.drop(vStations, 0, inplace=True)
        IDs, dists = get_geo_neighbors((c_lat, c_lon), geo_frame, center_radius, lat_field, lon_field, k=k_neighbors)
        allIDS.append(IDs)
        disdict.update(dict(zip(IDs, dists)))
        A = Analyze(run="dummy1/dummy2", RR_folder=file_folder)
        A.load_data(years, load_mode=[str(x) + "_" + var for x in IDs])
        subset_frame[A.X.columns] = A.X.loc[~A.X.index.duplicated(keep="first"), :]
        # filter out distorting Stations

    temp_frame = subset_frame.resample(s_rate).asfreq().interpolate('linear')
    subset_frame = temp_frame[temp_frame.index.year < int(test_year)]
    test_frame = temp_frame.loc[test_year:]
    # filter for complete records
    i_map = np.arange(subset_frame.shape[0])[sub_len + 1:-sub_len - 1]
    i_map = get_index_map(subset_frame, f"event@{sub_len + 1}", s_rate="10min", in_map=i_map)

    test_map = np.arange(test_frame.shape[0])[sub_len + 1:-sub_len - 1]
    test_map = get_index_map(test_frame, f"event@{sub_len + 1}", s_rate="10min", in_map=test_map)

    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################

    train_percentage = 0.8
    test_percentage = 0
    perm_train, perm_test = train_test_split(
        i_map, test_size=(1 - train_percentage), random_state=fix_seed
    )

    if test_percentage > 0:
        perm_test, perm_val = train_test_split(
            perm_test, test_size=(1 - test_percentage), random_state=fix_seed
        )
    else:
        perm_val = perm_test
        perm_test = np.array([], dtype=int)

    # make trafos
    trafo_dict = {}
    trafo_i = subset_frame.index[i_map]
    for v in subset_frame.columns:
        v_tüpp = v.split('_')[-1]
        if v_tüpp in norm_dict.keys():
            trafo_dict.update({v: makeTrafo(subset_frame.loc[trafo_i, v], norm_dict[v_tüpp])})
        else:
            trafo_dict.update({v: makeTrafo(subset_frame.loc[trafo_i, v], norm_dict['default'])})

    for key in trafo_dict.keys():
        trafo_dict.update({key: trafo_dict[str(c_id) + '_RR']})
    subset_frame = transformer(trafo_dict, subset_frame)
    test_frame = transformer(trafo_dict, test_frame)

    indäx = subset_frame.index
    train_i, val_i, test_i = (indäx[perm_train], indäx[perm_val], indäx[perm_test])

    # load data generators
    generator_ini_dict.update({"x_data": subset_frame.copy(),
                               "y_data": subset_frame[target_var].copy(),
                               "weights": None})

    train_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(perm_train),
        gen_index=perm_train,
    )
    train_generator.target_to_binary()
    # train_generator[0]

    val_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(perm_val),
        gen_index=perm_val,
    )
    val_generator.target_to_binary()

    generator_ini_dict.update({"x_data": test_frame.copy(),
                               "y_data": test_frame[target_var],
                               "weights": None})
    test_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(test_map),
        gen_index=test_map,
    )
    test_generator.target_to_binary()
    ####################################################################################################################
    # Prepare Experiment Storage / doc Experiment scenario
    ####################################################################################################################

    x_train, y_train = train_generator[0]
    x_val, y_val = val_generator[0]
    x_test, y_test = test_generator[0]
    w = (y_train == 0).sum() / (y_train == 1).sum()

    LR = [.1]
    MD = [3]
    NE = [1000]
    resultframe = pd.DataFrame([], columns=['DryDetect', 'WetDetect', 'LR', 'MD', 'NE'])
    k = 0
    for lr in LR:
        for md in MD:
            for ne in NE:
                model = XGBClassifier(objective="binary:logistic", learning_rate=lr, max_depth=md, n_estimators=ne,
                                      use_label_encoder=False, scale_pos_weight=w, eval_metric="error")

                class_mod = model.fit(x_train, y_train, eval_metric='error', eval_set=[(x_val, y_val)], verbose=True)
                prediction = class_mod.predict(x_test)
                confi, scores = scoreClassifier(y_test, class_mod.predict(x_test))
                resultframe.loc[k, :] = [scores['dry_detect'], scores['rain_detect'], lr, md, ne]
                print(k)
                print([scores['dry_detect'], scores['rain_detect'], lr, md, ne])
                k += 1

    print(scores)



if __name__ == "__main__":

    ####################################################################################################################
    # PARSING
    ####################################################################################################################

    #input('WARPING!')
    fix_seed = 145
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)

    c_id = 5600
    center_radius = 5
    k_neighbors = 5
    min_valids = 50000
    norm_dict = {'default': 'MinMax',
                 'RR': 'MinMax_qmaxx:0.99'}
    sub_len = 18
    s_rate = '10min'
    id_field = "STATIONS_ID"
    lat_field = "GEOGR_BREITE"
    lon_field = "GEOGR_LAENGE"
    stamp_field = "MESS_DATUM"
    field = "NIEDERSCHLAGSHOEHE"
    years = [
        "2010",
        "2011",
        "2012",
        "2013",
        "2014",
        "2015",
        "2016",
        "2017",
        "2018",
        "2019",
        "2020"
    ]
    #years = ['2014', '2020']
    test_year = '2020'

    target_var = f'{c_id}_RR'
    generator_ini_dict = {
        "sub_len": sub_len,
        "flatten": True,
        "pred_mode": "embedded",
        "pred_id": target_var,
        "miss_val": 0,
        "center_target": True,
        "rm_target_from_flat": True,
        "warp": False,
        "warp_fill": 'linear',
        "warp_range": 2
         }


    # retrieve center ID coordinates:
    geo_frame = pd.read_parquet(os.path.join("DWDData/" + "RR", "geoFrame.parquet"))
    c_lat = geo_frame.loc[c_id,:][0]
    c_lon = geo_frame.loc[c_id,:][1]
    subset_frame = pd.DataFrame([])
    allIDS = []
    disdict = {}
    var_load = tqdm(['RR'])#, '5600scpF'])
    for var in var_load:
        var_load.set_description(f"Loading {var}")
        folder_path = "DWDData/" + var
        file_folder = var
        geo_frame = pd.read_parquet(os.path.join(folder_path, "geoFrame.parquet"))
        count = getValidCount(folder_path, years=years)
        vStations = count[count < min_valids + 50000].index
        vStations = vStations.append(pd.Index([801]))
        # warn if target station is heavily underpopulated
        if (c_id in vStations) and (var=='RR'):
            print(f'Target Station (={c_id}), has less then the minimum number of valid values (={min_valids})!')
            vStations=vStations.drop(c_id)

        geo_frame.drop(vStations, 0, inplace=True)
        IDs, dists = get_geo_neighbors((c_lat, c_lon), geo_frame, center_radius, lat_field, lon_field, k=k_neighbors)
        allIDS.append(IDs)
        disdict.update(dict(zip(IDs, dists)))
        A = Analyze(run="dummy1/dummy2", RR_folder=file_folder)
        A.load_data(years, load_mode=[str(x) + "_" + var for x in IDs])
        subset_frame[A.X.columns] = A.X.loc[~A.X.index.duplicated(keep="first"), :]
        # filter out distorting Stations

    temp_frame = subset_frame.resample(s_rate).asfreq().interpolate('linear')
    subset_frame = temp_frame[temp_frame.index.year<int(test_year)]
    test_frame = temp_frame.loc[test_year:]
    # filter for complete records
    i_map = np.arange(subset_frame.shape[0])[sub_len + 1:-sub_len - 1]
    #i_map = get_index_map(subset_frame, sub_len + 1, s_rate="10min")
    #i_map = get_index_map(subset_frame, - sub_len - 1, s_rate="10min", in_map=i_map)
    i_map = get_index_map(subset_frame, f"event@{sub_len + 1}", s_rate="10min", in_map=i_map)

    #i_map = i_map[~i_na_mask]

    test_map = np.arange(test_frame.shape[0])[sub_len + 1:-sub_len-1]
    #test_map = get_index_map(test_frame, sub_len + 1, s_rate="10min")
    #test_map = get_index_map(test_frame, - sub_len - 1, s_rate="10min", in_map=test_map)
    test_map = get_index_map(test_frame, f"event@{sub_len + 1}", s_rate="10min", in_map=test_map)


    #test_map = test_map[~test_na_mask]
    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################


    train_percentage = 0.8
    test_percentage = 0
    perm_train, perm_test = train_test_split(
        i_map, test_size=(1 - train_percentage), random_state=fix_seed
    )

    if test_percentage > 0:
        perm_test, perm_val = train_test_split(
            perm_test, test_size=(1 - test_percentage), random_state=fix_seed
        )
    else:
        perm_val = perm_test
        perm_test = np.array([], dtype=int)

    # make trafos
    trafo_dict = {}
    trafo_i = subset_frame.index[i_map]
    for v in subset_frame.columns:
        v_tüpp = v.split('_')[-1]
        if v_tüpp in norm_dict.keys():
            trafo_dict.update({v: makeTrafo(subset_frame.loc[trafo_i, v], norm_dict[v_tüpp])})
        else:
            trafo_dict.update({v: makeTrafo(subset_frame.loc[trafo_i, v], norm_dict['default'])})

    for key in trafo_dict.keys():
        trafo_dict.update({key: trafo_dict[str(c_id) + '_RR']})
    subset_frame = transformer(trafo_dict, subset_frame)
    test_frame = transformer(trafo_dict, test_frame)

    indäx = subset_frame.index
    train_i, val_i, test_i = (indäx[perm_train], indäx[perm_val], indäx[perm_test])

    # load data generators
    generator_ini_dict.update({"x_data": subset_frame.copy(),
                               "y_data": subset_frame[target_var].copy(),
                               "weights": None})

    train_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(perm_train),
        gen_index=perm_train,
    )
    train_generator.target_to_binary()
    #train_generator[0]


    val_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(perm_val),
        gen_index=perm_val,
    )
    val_generator.target_to_binary()

    generator_ini_dict.update({"x_data": test_frame.copy(),
                               "y_data": test_frame[target_var],
                               "weights": None})
    test_generator = TSGen(
        **generator_ini_dict,
        batch_size=len(test_map),
        gen_index=test_map,
    )
    test_generator.target_to_binary()
    ####################################################################################################################
    # Prepare Experiment Storage / doc Experiment scenario
    ####################################################################################################################

    x_train, y_train = train_generator[0]
    x_val, y_val = val_generator[0]
    x_test, y_test = test_generator[0]
    w=(y_train==0).sum()/(y_train==1).sum()

    LR = [.1]
    MD = [3]
    NE = [1000]
    resultframe = pd.DataFrame([], columns=['DryDetect', 'WetDetect', 'LR', 'MD', 'NE'])
    k=0
    for lr in LR:
        for md in MD:
            for ne in NE:
                model = XGBClassifier(objective="binary:logistic", learning_rate=lr, max_depth=md, n_estimators=ne,
                          use_label_encoder=False, scale_pos_weight=w, eval_metric="error")

                class_mod = model.fit(x_train, y_train, eval_metric='error', eval_set=[(x_val,y_val)],verbose=True)
                prediction = class_mod.predict(x_test)
                confi, scores = scoreClassifier(y_test, class_mod.predict(x_test))
                resultframe.loc[k,:] = [scores['dry_detect'], scores['rain_detect'], lr, md, ne]
                print(k)
                print([scores['dry_detect'], scores['rain_detect'], lr, md, ne])
                k+=1

    print(scores)
    #generator_ini_dict['warp'] = False
    #train_generator_plain = TSGen(
    #    **generator_ini_dict,
    #    batch_size=len(perm_train),
    #    gen_index=perm_train,
    #)
    #train_generator_plain.target_to_binary()
    #test_generator_plain = TSGen(
    #    **generator_ini_dict,
    #    batch_size=len(test_map),
    #    gen_index=test_map,
    #)
    #test_generator_plain.target_to_binary()
    #x_plain_train, y_plain_train = train_generator_plain[0]
    #x_plain_test, y_plain_test = test_generator_plain[0]
    #full_train = np.concatenate((x_train, x_plain_train), axis=1)
    #full_test = np.concatenate((x_test, x_plain_test), axis=1)
    #rain_i=test_generator.y_data.index[test_generator.gen_index][prediction.astype(bool)]
    #rain_i.to_series().to_csv('/home/luenensc/PyPojects/NeuraLab/Results/classification/rain_i_dtw_bench.csv')
    #val_ser = test_frame.loc[:, '5600_RR']
    #detected_ser = pd.Series(0, index=test_frame.loc[:, '5600_RR'].index)
    #detected_ser[rain_i] = 10
    #plt.fill_between(detected_ser.index, detected_ser.values, step="mid", alpha=0.4, label='class prediction')
    #plt.plot(val_ser.index, val_ser.values, label='precipitation')
    #plt.title('Class Prediction for ID5600')
    #plt.ylabel('precipitation')
    #plt.legend()


