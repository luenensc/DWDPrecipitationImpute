import pandas as pd
import matplotlib.pyplot as plt
import scipy.spatial
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
from keras import optimizers
from keras import models
from keras import layers
import keras
import os
import random
import pickle
import matplotlib as mpl
import funcs


def plot_prediction_series(
    model,
    x: pd.DataFrame,
    y: pd.Series,
    miss_mask: pd.DataFrame,
    scales,
    mode="series",
    save_path=None,
):
    # if save_path:
    #    mpl.use("Agg")

    new_i, indexer = y.index.sort_values(return_indexer=True)
    y = y.loc[new_i]
    y_miss_mask = miss_mask.loc[:, y.name]
    prediction = model.predict(scaler(scales, x, miss_mask, miss_val=-1))
    if prediction.shape[1] > 1:
        # prediction = prediction[:, x.columns.get_loc(y.name)]
        prediction = pd.DataFrame(
            prediction[:, x.columns.get_loc(y.name)], columns=[y.name]
        )
        prediction = scaler(
            scales, prediction, miss_mask, miss_val=-1, mode="rescale"
        ).squeeze()
    else:
        prediction = pd.DataFrame(prediction, columns=[y.name])
        prediction = scaler(
            scales, prediction, miss_mask, miss_val=-1, mode="rescale"
        ).squeeze()
        # prediction = prediction[:, 0]
    prediction = pd.Series(prediction.values, index=y.index)
    error = y - prediction
    if not miss_mask.empty:
        y_miss = y[y_miss_mask]
        error_miss = error[y_miss_mask]

    prediction = pd.Series(prediction[indexer], index=new_i)

    if mode == "series":
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True)
        axes[0].plot(y, label="observation")
        axes[0].plot(prediction, label="prediction")
        axes[0].set_title("Station " + str(y.name))
        axes[0].set_ylabel("Precipitation[mm]")
        if not miss_mask.empty:
            axes[0].scatter(y_miss.index, y_miss.values, color="r", label="Imputed")
        axes[0].legend()
        axes[1].plot(y - prediction)
        if not miss_mask.empty:
            axes[1].scatter(error_miss.index, error_miss.values, color="r")
        axes[1].set_ylabel("Error")
    if mode == "scatter":
        fig = plt.figure()
        plt.scatter(y.values, prediction.values)
        if not miss_mask.empty:
            plt.scatter(
                y.values[y_miss_mask], prediction.values[y_miss_mask], color="r"
            )
        m = max(y.max(), prediction.max())
        plt.plot([0, m], [0, m], color="black")
        plt.axis("square")
        plt.xlabel("observation")
        plt.ylabel("prediction")

    if save_path:
        if os.path.splitext(save_path)[1] == ".pickle":
            with open(save_path, "wb") as f:
                pickle.dump(fig, f)
        else:
            fig.savefig(save_path)

    return fig


def plot_stations(geo_frame, col="b"):
    figure = plt.scatter(
        geo_frame[lat_field].values, geo_frame[lon_field].values, color=col
    )
    return figure


def plot_missing(station):
    na_mask = station.isna()
    filled_station = station.interpolate("time")
    na_vals = filled_station[na_mask]
    figure = plt.plot(station)
    plt.scatter(na_vals.index, na_vals.values, color="r")
    return figure


def plot_train_history(history: dict, metric: str):
    monitor = history[metric]
    val_monitor = history["val_" + metric]

    epochs = range(1, len(monitor) + 1)
    # "bo" is for "blue dot"
    plt.plot(epochs, monitor, "bo", label="Training " + metric)
    # b is for "solid blue line"
    plt.plot(epochs, val_monitor, "b", label="Validation " + metric)
    plt.title("Training and validation monitor on " + metric)
    plt.xlabel("Epochs")
    plt.ylabel(metric)
    plt.legend()
    plt.show()


def get_geo_neighbors(id, geo_frame, dist, k=None):
    dist_ser = np.sqrt(
        (geo_frame[lat_field] - geo_frame.loc[id, lat_field]) ** 2
        + (geo_frame[lon_field] - geo_frame.loc[id, lon_field]) ** 2
    )
    neighbors = dist_ser < dist
    neighbors = neighbors[neighbors].index

    if k:
        nearest = dist_ser[neighbors].argsort()[:k]
        neighbors = neighbors[nearest]

    return neighbors


def get_dist_mat(meas_frame, metric):
    dist_mat = scipy.spatial.distance.squareform(
        scipy.spatial.distance.pdist(meas_frame.values.T, metric)
    )
    return pd.DataFrame(dist_mat, columns=meas_frame.columns, index=meas_frame.columns)


def get_dist_neighbors(dist_frame, id, thresh=None, k=None):
    if thresh:
        neighbors = dist_frame[id] < thresh
        neighbors = neighbors[neighbors].index
    else:
        neighbors = dist_frame.index
    if k:
        nearest = dist_frame.loc[id, neighbors].values.argsort()[:k]
        neighbors = neighbors[nearest]
    return neighbors


def get_full_ts(meas_frame, ids=None):
    if ids is not None:
        meas_frame = meas_frame.copy()
        meas_frame = meas_frame.loc[:, ids]
    nona = meas_frame.isna().any(axis=1) == False
    return nona[nona].index


def corrupt(meas_frame, c_func, c_val):
    """
    :param c_func:  Function, determining which values to mark as missing. be a function of pandas timeseries and return
                    boolean array.
    :return:
    """
    to_corrupt = pd.DataFrame(
        data=False, columns=meas_frame.columns, index=meas_frame.index
    )
    vals = np.apply_along_axis(lambda row: c_func(row), 1, to_corrupt.values)
    to_corrupt.loc[:, :] = vals
    corrupted = meas_frame.copy()
    corrupted[to_corrupt] = c_val

    return meas_frame, corrupted, to_corrupt


# def get_hinted_data()


def random_drop(x, drop_dist):
    drop_frac = np.random.choice(drop_dist[0], p=drop_dist[1])
    drop_num = np.round(drop_frac * len(x))
    try:
        drop_i = np.random.choice(len(x), int(drop_num), replace=False)
    except ValueError:
        drop_i = np.random.choice(len(x), int(drop_num) - 1, replace=False)
    out_arr = np.array([False] * len(x))
    out_arr[drop_i] = True
    return out_arr


def get_miss_dist(meas_frame):
    na_per_ts = (meas_frame.isna().sum(axis=1)).values
    hist = np.histogram(na_per_ts, bins=np.arange(max(na_per_ts) + 1))
    return hist[1][:-1] / len(meas_frame.columns), hist[0] / sum(hist[0])


def make_model(
    layer_list,
    input_dim,
    loss="mean_squared_error",
    monitors=["mae"],
    seed=None,
    optimizer=optimizers.Adam(lr=1e-3),
):

    keras.backend.clear_session()
    initializer = keras.initializers.glorot_normal(seed=seed)
    model = models.Sequential()
    n = 0
    for lay in layer_list:
        if isinstance(lay, dict):
            lay_typ = lay["type"].pop
            if "initializer" in lay.keys():
                lay["kernel_initializer"] = initializer
                lay_dict = lay
        if isinstance(lay, str):
            split = lay.find("_")
            lt = lay[split + 1 :]
            larg = lay[:split]
            if lt == "drop":
                lay_typ = "Dropout"
                lay_dict = {"rate": float(larg)}
                lay_dict.update({"seed": seed})

        if isinstance(lay, int):
            lay_typ = "Dense"
            lay_dict = {"units": lay}
            lay_dict.update({"kernel_initializer": initializer})
            if n == 0:
                lay_dict.update({"input_dim": input_dim})
            if n == (len(layer_list) - 1):
                lay_dict.update({"activation": "linear"})
            else:
                lay_dict.update({"activation": "relu"})
        n += 1

        model.add(getattr(layers, lay_typ)(**lay_dict))

    model.compile(optimizer=optimizer, loss=loss, metrics=monitors)
    return model


def eval_at_missing(
    model,
    x: pd.DataFrame,
    y: pd.Series,
    miss_mask: pd.DataFrame,
    scales=None,
    metric="mae",
):
    y_miss = miss_mask.loc[:, y.name]
    pred_y = model.predict(scaler(scales, x, miss_mask, miss_val=-1))
    if pred_y.shape[1] > 1:
        pred_y = pd.DataFrame(pred_y[:, x.columns.get_loc(y.name)], columns=[y.name])
        pred_y = scaler(
            scales, pred_y, miss_mask, miss_val=-1, mode="rescale"
        ).squeeze()
        # pred_y = pred_y[:, x.columns.get_loc(y.name)]*scales[y.name]
    else:
        pred_y = pd.DataFrame(pred_y, columns=[y.name])
        pred_y = scaler(
            scales, pred_y, miss_mask, miss_val=-1, mode="rescale"
        ).squeeze()
    pred_y = pd.Series(pred_y.values, index=y.index)
    if metric == "mae":
        result = (abs(pred_y[y_miss] - y[y_miss])).sum().sum() / (y_miss.sum().sum())
    return result


def scaler(scale, to_scale, miss_mask, mode="scale", miss_val=-1):
    if mode == "scale":
        to_scale = to_scale / scale[to_scale.columns]
        to_scale.update(miss_mask.replace({True: miss_val, False: np.nan}))
    if mode == "rescale":
        to_scale = to_scale * scale[to_scale.columns]
        to_scale.update(miss_mask.replace({True: miss_val, False: np.nan}))
    return to_scale


if __name__ == "__main__":

    ####################################################################################################################
    # PARSING
    ####################################################################################################################
    fix_seed = 36
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)
    center_id = 44
    center_radius = 2
    n_rows = None
    id_field = "STATIONS_ID"
    lat_field = "GEOGR_BREITE"
    lon_field = "GEOGR_LAENGE"
    stamp_field = "MESS_DATUM"
    field = "NIEDERSCHLAGSHOEHE"
    years = ["2015", "2016", "2017"]
    folder_path = "/home/luenensc/PyPojects/testSpace/DWDData/RR/"
    subset_frame = corrupted_frame = corruption_mask = pd.DataFrame([])
    for y in years:
        path = os.path.join(folder_path, y + ".csv")
        if n_rows:
            data = pd.read_csv(path, nrows=n_rows)
        else:
            data = pd.read_csv(path)

        m_frame = data.pivot_table(index=stamp_field, columns=id_field, values=field)
        m_frame.index = pd.DatetimeIndex(m_frame.index)
        geo_frame = (
            data[[id_field, lat_field, lon_field]].drop_duplicates().set_index(id_field)
        )

        ####################################################################################################################
        # SENSOR SUBNET FRAMING
        ####################################################################################################################

        IDs = get_geo_neighbors(center_id, geo_frame, center_radius)
        if 760 in IDs:
            IDs = IDs.drop(760)
        sub_frame = m_frame.loc[:, IDs]
        center_index = list(sub_frame.columns).index(center_id)
        ####################################################################################################################
        # MISS DIST ANALYSIS AND REPRODUCTION
        ####################################################################################################################

        na_num, na_dist = get_miss_dist(m_frame)
        complete_stamps = get_full_ts(sub_frame)
        sub_frame, corr_frame, corr_mask = corrupt(
            sub_frame.loc[complete_stamps, :],
            lambda x: random_drop(x, (na_num, na_dist)),
            -1,
        )

        if subset_frame.empty:
            subset_frame, corrupted_frame, corruption_mask = (
                sub_frame,
                corr_frame,
                corr_mask,
            )
        else:
            subset_frame = pd.concat([subset_frame, sub_frame])
            corrupted_frame = pd.concat([corrupted_frame, corr_frame])
            corruption_mask = pd.concat([corruption_mask, corr_mask])
    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################
    scales = subset_frame.max()
    scales[scales == 0] = 1

    train_percentage = 0.8
    test_percentage = 0.5
    x_train, x_test, y_train, y_test = train_test_split(
        scaler(scales, corrupted_frame, corruption_mask),
        subset_frame / scales,
        test_size=(1 - train_percentage),
        random_state=fix_seed,
    )

    x_test, x_val, y_test, y_val = train_test_split(
        x_test, y_test, test_size=(1 - test_percentage), random_state=fix_seed
    )

    x_test_i, x_val_i, y_test_i, y_val_i, x_train_i, y_train_i = (
        x_test.index,
        x_val.index,
        y_test.index,
        y_val.index,
        x_train.index,
        y_train.index,
    )
    # x_test, x_val, y_test, y_val = x_test.values, x_val.values, y_test.values, y_val.values
    ####################################################################################################################
    # Prepare Experiment Storage / doc Experiment scenario
    ####################################################################################################################
    Model_Num = 0
    Exp_folder = "/home/luenensc/PyPojects/NeuraLab/Results/spacial3Y"
    if not os.path.exists(Exp_folder):
        os.mkdir(Exp_folder)
    info = pd.Series({"center_id": center_id, "radius": 2, "seed": fix_seed})
    info.to_csv(os.path.join(Exp_folder, "info.csv"))
    corruption_mask.to_csv(os.path.join(Exp_folder, "miss_mask.csv"))
    subset_frame.to_csv(os.path.join(Exp_folder, "preci_data.csv"))
    x_val_i.to_series().to_csv(os.path.join(Exp_folder, "validation_index.csv"))
    x_test_i.to_series().to_csv(os.path.join(Exp_folder, "test_index.csv"))
    ####################################################################################################################
    # Experiment 1.1: plain simultane model
    ####################################################################################################################
    exp_name = "plain_sim"
    exp_path = os.path.join(Exp_folder, exp_name)
    if not os.path.exists(exp_path):
        os.mkdir(exp_path)

    n_inputs = x_train.shape[1]  # 318
    loss = "mean_squared_error"
    metrics = [funcs.ccc, "mae", funcs.r2]
    scores = []
    moddels = [
        [n_inputs, n_inputs],
        [n_inputs, str(0.5) + "_drop", n_inputs],
        [n_inputs, n_inputs, n_inputs],
        [n_inputs, str(0.5) + "_drop", n_inputs, str(0.5) + "_drop", n_inputs],
        [n_inputs, n_inputs, n_inputs, n_inputs],
        [
            n_inputs,
            str(0.5) + "_drop",
            n_inputs,
            str(0.5) + "_drop",
            n_inputs,
            str(0.5) + "_drop",
            n_inputs,
        ],
    ]

    # model_single = make_model([n_inputs-1, n_inputs-1, round(n_inputs/2), round(n_inputs/4), 1],
    #                           input_dim=n_inputs-1,
    #                           loss=loss,
    #                           monitors=[funcs.ccc,"mae",funcs.r2],
    #                           seed=fix_seed,
    #                           optimizer=optimizers.Adam(lr=1e-3))

    ####################################################################################################################
    # Training Model
    ####################################################################################################################
    for mod in moddels:
        for train_para in [(100, 10), (200, 20), (1000, 200), (2000, 2000)]:
            m = make_model(
                mod,
                input_dim=n_inputs,
                loss=loss,
                monitors=metrics,
                seed=fix_seed,
                optimizer=optimizers.Adam(lr=1e-3),
            )

            n_epochs = train_para[0]
            batch_size = train_para[1]
            history = m.fit(
                x_train,
                y_train,
                epochs=n_epochs,
                batch_size=batch_size,
                validation_data=(x_val, y_val),
            )

            with open(
                os.path.join(exp_path, str(Model_Num) + "_history.pickle"), "wb"
            ) as f:
                pickle.dump(history.history, f)
            m.save(os.path.join(exp_path, str(Model_Num) + "_model"))

            plot_prediction_series(
                m,
                corrupted_frame,
                subset_frame.loc[:, 44],
                corruption_mask,
                scales,
                save_path=os.path.join(exp_path, str(Model_Num) + "_pred_ser.pickle"),
            )
            plot_prediction_series(
                m,
                corrupted_frame,
                subset_frame.loc[:, 44],
                corruption_mask,
                scales,
                mode="scatter",
                save_path=os.path.join(
                    exp_path, str(Model_Num) + "_pred_scatter.pickle"
                ),
            )

            score = eval_at_missing(
                m,
                corrupted_frame.loc[x_val_i, :],
                subset_frame.loc[x_val_i, center_id],
                corruption_mask.loc[x_val_i, :],
                scales,
            )

            info = pd.Series(
                {"batch_size": batch_size, "epochs": n_epochs, "score": score}
            )
            scores.append(score)
            info.to_csv(os.path.join(exp_path, str(Model_Num) + "_score"))
            Model_Num += 1
    pd.Series(scores).to_csv(os.path.join(exp_path, "all_scores.csv"))

    ####################################################################################################################
    # Experiment 1.2: hinted simultane model
    ####################################################################################################################

    exp_name = "hinted_sim"
    exp_path = os.path.join(Exp_folder, exp_name)
    if not os.path.exists(exp_path):
        os.mkdir(exp_path)
    hints = corruption_mask.astype(int)

    # Add hints:
    hint_cols = [str(x) + "_hint" for x in hints.columns]
    x_train[hint_cols] = hints.loc[x_train_i, :].values
    x_val[hint_cols] = hints.loc[x_val_i, :].values
    x_test[hint_cols] = hints.loc[x_test_i, :].values
    scales = scales.append(pd.Series(1, index=hint_cols))

    corrupted_frame[hint_cols] = hints.values
    subset_frame[hint_cols] = hints.values
    corruption_mask[hint_cols] = False

    n_inputs = x_train.shape[1]  # 318
    loss = "mean_squared_error"
    metrics = [funcs.ccc, "mae", funcs.r2]
    scores = []
    Model_Num = 0
    moddels = [
        [n_inputs, int(n_inputs / 2)],
        [n_inputs, str(0.5) + "_drop", int(round(n_inputs * 0.5))],
        [n_inputs, int(round(n_inputs * 3 / 4)), int(round(n_inputs * 0.5))],
        [
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 3 / 4)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.5)),
        ],
        [n_inputs, n_inputs, int(round(n_inputs * 3 / 4)), int(round(n_inputs * 0.5))],
        [
            n_inputs,
            str(0.5) + "_drop",
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 3 / 4)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.5)),
        ],
        [
            n_inputs,
            int(round(n_inputs * 3 / 4)),
            int(round(n_inputs * 0.5)),
            int(round(n_inputs * 0.5)),
        ],
        [
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 3 / 4)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.5)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.5)),
        ],
    ]

    for mod in moddels:
        for train_para in [(100, 1), (200, 20), (1000, 200), (2000, 2000)]:
            m = make_model(
                mod,
                input_dim=n_inputs,
                loss=loss,
                monitors=metrics,
                seed=fix_seed,
                optimizer=optimizers.Adam(lr=1e-3),
            )

            n_epochs = train_para[0]
            batch_size = train_para[1]
            history = m.fit(
                x_train,
                y_train,
                epochs=n_epochs,
                batch_size=batch_size,
                validation_data=(x_val, y_val),
            )

            with open(
                os.path.join(exp_path, str(Model_Num) + "_history.pickle"), "wb"
            ) as f:
                pickle.dump(history.history, f)
            m.save(os.path.join(exp_path, str(Model_Num) + "_model"))
            plot_prediction_series(
                m,
                corrupted_frame,
                subset_frame.loc[:, 44],
                corruption_mask,
                scales,
                save_path=os.path.join(exp_path, str(Model_Num) + "_pred_ser.pickle"),
            )
            plot_prediction_series(
                m,
                corrupted_frame,
                subset_frame.loc[:, 44],
                corruption_mask,
                scales,
                mode="scatter",
                save_path=os.path.join(
                    exp_path, str(Model_Num) + "_pred_scatter.pickle"
                ),
            )
            score = eval_at_missing(
                m,
                corrupted_frame.loc[x_val_i, :],
                subset_frame.loc[x_val_i, center_id],
                corruption_mask.loc[x_val_i, :],
                scales,
            )

            info = pd.Series(
                {"batch_size": batch_size, "epochs": n_epochs, "score": score}
            )
            scores.append(score)
            info.to_csv(os.path.join(exp_path, str(Model_Num) + "_score"))
            Model_Num += 1

    pd.Series(scores).to_csv(os.path.join(exp_path, "all_scores.csv"))

    print("stop")
    # - add r2,ccc monitor to reported scores
    #
