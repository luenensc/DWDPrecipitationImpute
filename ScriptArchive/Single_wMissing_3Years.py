import pandas as pd
import matplotlib.pyplot as plt
import scipy.spatial
import numpy as np
from sklearn.model_selection import train_test_split
import tensorflow as tf
from keras import optimizers
from keras import models
from keras import layers
from plotting import (
    plot_train_history,
    plot_prediction_series,
    plot_missing,
    plot_stations,
)
from monitoring import r2, ccc, eval_at_missing
from preprocessing import *
from modelConstrunction import *
import keras
import os
import random
import pickle


if __name__ == "__main__":

    ####################################################################################################################
    # PARSING
    ####################################################################################################################
    fix_seed = 36
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)
    center_id = 44
    center_radius = 2
    n_rows = None
    id_field = "STATIONS_ID"
    lat_field = "GEOGR_BREITE"
    lon_field = "GEOGR_LAENGE"
    stamp_field = "MESS_DATUM"
    field = "NIEDERSCHLAGSHOEHE"
    years = ["2015", "2016", "2017"]
    folder_path = "DWDData/RRSparse/"
    subset_frame = corrupted_frame = corruption_mask = pd.DataFrame([])
    for y in years:
        path = os.path.join(folder_path, y + ".csv")
        if n_rows:
            data = pd.read_csv(path, nrows=n_rows)
        else:
            data = pd.read_csv(path)

        m_frame = data.pivot_table(index=stamp_field, columns=id_field, values=field)
        m_frame.index = pd.DatetimeIndex(m_frame.index)
        geo_frame = (
            data[[id_field, lat_field, lon_field]].drop_duplicates().set_index(id_field)
        )

        ####################################################################################################################
        # SENSOR SUBNET FRAMING
        ####################################################################################################################

        IDs = get_geo_neighbors(
            center_id, geo_frame, center_radius, lat_field, lon_field
        )
        if 760 in IDs:
            IDs = IDs.drop(760)
        sub_frame = m_frame.loc[:, IDs]
        center_index = list(sub_frame.columns).index(center_id)
        ####################################################################################################################
        # MISS DIST ANALYSIS AND REPRODUCTION
        ####################################################################################################################

        na_num, na_dist = get_miss_dist(m_frame)
        complete_stamps = get_full_ts(sub_frame)
        sub_frame, corr_frame, corr_mask = corrupt(
            sub_frame.loc[complete_stamps, :],
            lambda x: random_drop(x, (na_num, na_dist)),
            -1,
        )

        if subset_frame.empty:
            subset_frame, corrupted_frame, corruption_mask = (
                sub_frame,
                corr_frame,
                corr_mask,
            )
        else:
            subset_frame = pd.concat([subset_frame, sub_frame])
            corrupted_frame = pd.concat([corrupted_frame, corr_frame])
            corruption_mask = pd.concat([corruption_mask, corr_mask])
    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################
    scales = subset_frame.max()
    scales[scales == 0] = 1

    train_percentage = 0.8
    test_percentage = 0.5
    x_train, x_test, y_train, y_test = train_test_split(
        scaler(scales, corrupted_frame, corruption_mask),
        subset_frame / scales,
        test_size=(1 - train_percentage),
        random_state=fix_seed,
    )

    x_test, x_val, y_test, y_val = train_test_split(
        x_test, y_test, test_size=(1 - test_percentage), random_state=fix_seed
    )

    x_test_i, x_val_i, y_test_i, y_val_i, x_train_i, y_train_i = (
        x_test.index,
        x_val.index,
        y_test.index,
        y_val.index,
        x_train.index,
        y_train.index,
    )
    x_test, x_val, x_train = (
        x_test.drop(center_id, 1),
        x_val.drop(center_id, 1),
        x_train.drop(center_id, 1),
    )
    y_test, y_val, y_train = y_test[center_id], y_val[center_id], y_train[center_id]
    ####################################################################################################################
    # Prepare Experiment Storage / doc Experiment scenario
    ####################################################################################################################

    Exp_folder = "Results/currentRun"
    if not os.path.exists(Exp_folder):
        os.mkdir(Exp_folder)

    ####################################################################################################################
    # Run
    ####################################################################################################################
    Model_Num = 0
    run_name = "plain_single"
    run_path = os.path.join(Exp_folder, run_name)
    if not os.path.exists(run_path):
        os.mkdir(run_path)

    ########################################
    # Generate reconstruction Info for the run:
    ########################################
    info = pd.Series({"center_id": center_id, "radius": 2, "seed": fix_seed})

    info.to_csv(os.path.join(run_path, "info.csv"))
    c_temp = corruption_mask.columns
    corruption_mask.columns = [str(x) for x in c_temp]
    corruption_mask.to_parquet(os.path.join(run_path, "miss_mask.parquet"))
    corruption_mask.columns = c_temp
    IDs.to_series().to_csv(os.path.join(run_path, "IDs.csv"), index=False)
    x_train_i.to_series().to_csv(os.path.join(run_path, "train_index.csv"), index=False)
    x_val_i.to_series().to_csv(
        os.path.join(run_path, "validation_index.csv"), index=False
    )
    x_test_i.to_series().to_csv(os.path.join(run_path, "test_index.csv"), index=False)
    pd.DataFrame(x_train).columns.to_series().to_csv(
        os.path.join(run_path, "IDx.csv"), index=False
    )
    pd.DataFrame(y_train).columns.to_series().to_csv(
        os.path.join(run_path, "IDy.csv"), index=False
    )
    scales.to_csv(os.path.join(run_path, "scales.csv"))

    n_inputs = x_train.shape[1]  # 318
    loss = "mean_squared_error"
    metrics = [ccc, "mae", r2]
    scores = pd.DataFrame([])
    moddels = [
        [n_inputs, 1],
        [n_inputs, str(0.5) + "_drop", 1],
        [n_inputs, int(round(n_inputs / 2)), 1],
        [n_inputs, str(0.5) + "_drop", int(round(n_inputs / 2)), str(0.5) + "_drop", 1],
        [n_inputs, int(round(n_inputs * 0.66)), int(round(n_inputs * 0.33)), 1],
        [
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 0.66)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.33)),
            str(0.5) + "_drop",
            1,
        ],
    ]
    moddels = []
    ####################################################################################################################
    # Training Model
    ####################################################################################################################

    for mod in moddels:
        for train_para in [(100, 10), (200, 20), (1000, 200), (2000, 2000)]:
            m = make_model(
                mod,
                input_dim=n_inputs,
                loss=loss,
                monitors=metrics,
                seed=fix_seed,
                optimizer=optimizers.Adam(lr=1e-3),
            )

            n_epochs = train_para[0]
            batch_size = train_para[1]
            history = m.fit(
                x_train,
                y_train,
                epochs=n_epochs,
                batch_size=batch_size,
                validation_data=(x_val, y_val),
            )

            with open(
                os.path.join(run_path, str(Model_Num) + "_history.pickle"), "wb"
            ) as f:
                pickle.dump(history.history, f)

            with open(
                os.path.join(run_path, str(Model_Num) + "_summary.txt"), "w"
            ) as fh:
                m.summary(print_fn=lambda x: fh.write(x + "\n"))

            m.save(os.path.join(run_path, str(Model_Num) + "_model"))

            plot_prediction_series(
                m,
                corrupted_frame.drop(center_id, 1),
                subset_frame.loc[:, center_id],
                corruption_mask,
                scales,
                save_path=os.path.join(run_path, str(Model_Num) + "_pred_ser.pickle"),
            )
            plot_prediction_series(
                m,
                corrupted_frame.drop(center_id, 1),
                subset_frame.loc[:, center_id],
                corruption_mask,
                scales,
                mode="scatter",
                save_path=os.path.join(
                    run_path, str(Model_Num) + "_pred_scatter.pickle"
                ),
            )
            plot_prediction_series(
                m,
                corrupted_frame.drop(center_id, 1),
                subset_frame.loc[:, center_id],
                corruption_mask,
                scales,
                mode="scatter",
                save_path=os.path.join(run_path, str(Model_Num) + "_pred_scatter.png"),
            )

            score = eval_at_missing(
                m,
                corrupted_frame.loc[x_val_i, :].drop(center_id, 1),
                subset_frame.loc[x_val_i, center_id],
                corruption_mask.loc[x_val_i, :],
                scales,
            )

            res = pd.DataFrame(
                {
                    "score_on_val_miss": score,
                    "loss_val_min": min(history.history["val_loss"]),
                    "loss_val_final": history.history["val_loss"][-1],
                    "r2_val_max": max(history.history["val_r2"]),
                    "r2_final": history.history["val_r2"][-1],
                    "ccc_val_max": max(history.history["ccc"]),
                    "ccc_val_final": history.history["ccc"][-1],
                    "epochs": n_epochs,
                    "batch": batch_size,
                },
                index=[Model_Num],
            )

            scores = scores.append(res)
            res.to_csv(os.path.join(run_path, str(Model_Num) + "_score.csv"))
            Model_Num += 1
    if not scores.empty:
        scores.to_csv(os.path.join(run_path, "all_scores.csv"))

    ####################################################################################################################
    # Experiment 1.2: hinted single model
    ####################################################################################################################

    run_name = "hinted_single"
    run_path = os.path.join(Exp_folder, run_name)
    if not os.path.exists(run_path):
        os.mkdir(run_path)
    hints = corruption_mask.astype(int)

    # Add hints:
    hint_cols_d = [str(x) + "_hint" for x in hints.columns.drop(center_id)]
    hint_cols = [str(x) + "_hint" for x in hints.columns]
    x_train[hint_cols_d] = hints.loc[x_train_i, :].drop(center_id, 1).values
    x_val[hint_cols_d] = hints.loc[x_val_i, :].drop(center_id, 1).values
    x_test[hint_cols_d] = hints.loc[x_test_i, :].drop(center_id, 1).values

    scales = scales.append(pd.Series(1, index=hint_cols))
    corrupted_frame[hint_cols] = hints.values
    subset_frame[hint_cols] = hints.values
    corruption_mask[hint_cols] = False

    info = pd.Series({"center_id": center_id, "radius": 2, "seed": fix_seed})

    info.to_csv(os.path.join(run_path, "info.csv"))
    c_temp = corruption_mask.columns
    corruption_mask.columns = [str(x) for x in c_temp]
    corruption_mask.to_parquet(os.path.join(run_path, "miss_mask.parquet"))
    corruption_mask.columns = c_temp
    IDs.to_series().to_csv(os.path.join(run_path, "IDs.csv"), index=False)
    x_train_i.to_series().to_csv(os.path.join(run_path, "train_index.csv"), index=False)
    x_val_i.to_series().to_csv(
        os.path.join(run_path, "validation_index.csv"), index=False
    )
    x_test_i.to_series().to_csv(os.path.join(run_path, "test_index.csv"), index=False)
    pd.DataFrame(x_train).columns.to_series().to_csv(
        os.path.join(run_path, "IDx.csv"), index=False
    )
    pd.DataFrame(y_train).columns.to_series().to_csv(
        os.path.join(run_path, "IDy.csv"), index=False
    )
    scales.to_csv(os.path.join(run_path, "scales.csv"))

    n_inputs = x_train.shape[1]  # 318
    loss = "mean_squared_error"
    metrics = [ccc, "mae", r2]
    scores = pd.DataFrame([])
    Model_Num = 0

    moddels = [
        [n_inputs, 1],
        [n_inputs, str(0.5) + "_drop", 1],
        [n_inputs, int(round(n_inputs * 0.5)), 1],
        [
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 0.5)),
            str(0.5) + "_drop",
            1,
        ],
        [n_inputs, n_inputs, int(round(n_inputs * 0.5)), 1],
        [
            n_inputs,
            str(0.5) + "_drop",
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 0.5)),
            str(0.5) + "_drop",
            1,
        ],
        [n_inputs, int(round(n_inputs * 0.66)), int(round(n_inputs * 0.33)), 1],
        [
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 0.66)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.33)),
            str(0.5) + "_drop",
            1,
        ],
    ]
    moddels = []
    for mod in moddels:
        for train_para in [(100, 10), (200, 20), (1000, 200), (2000, 2000)]:
            m = make_model(
                mod,
                input_dim=n_inputs,
                loss=loss,
                monitors=metrics,
                seed=fix_seed,
                optimizer=optimizers.Adam(lr=1e-3),
            )

            n_epochs = train_para[0]
            batch_size = train_para[1]
            history = m.fit(
                x_train,
                y_train,
                epochs=n_epochs,
                batch_size=batch_size,
                validation_data=(x_val, y_val),
            )

            with open(
                os.path.join(run_path, str(Model_Num) + "_history.pickle"), "wb"
            ) as f:
                pickle.dump(history.history, f)

            with open(
                os.path.join(run_path, str(Model_Num) + "_summary.txt"), "w"
            ) as fh:
                m.summary(print_fn=lambda x: fh.write(x + "\n"))

            m.save(os.path.join(run_path, str(Model_Num) + "_model"))

            plot_prediction_series(
                m,
                corrupted_frame.drop([center_id, str(center_id) + "_hint"], 1),
                subset_frame.loc[:, center_id],
                corruption_mask,
                scales,
                save_path=os.path.join(run_path, str(Model_Num) + "_pred_ser.pickle"),
            )
            plot_prediction_series(
                m,
                corrupted_frame.drop([center_id, str(center_id) + "_hint"], 1),
                subset_frame.loc[:, center_id],
                corruption_mask,
                scales,
                mode="scatter",
                save_path=os.path.join(
                    run_path, str(Model_Num) + "_pred_scatter.pickle"
                ),
            )
            plot_prediction_series(
                m,
                corrupted_frame.drop([center_id, str(center_id) + "_hint"], 1),
                subset_frame.loc[:, center_id],
                corruption_mask,
                scales,
                mode="scatter",
                save_path=os.path.join(run_path, str(Model_Num) + "_pred_scatter.png"),
            )

            score = eval_at_missing(
                m,
                corrupted_frame.loc[x_val_i, :].drop(
                    [center_id, str(center_id) + "_hint"], 1
                ),
                subset_frame.loc[x_val_i, center_id],
                corruption_mask.loc[x_val_i, :],
                scales,
            )

            res = pd.DataFrame(
                {
                    "score_on_val_miss": score,
                    "loss_val_min": min(history.history["val_loss"]),
                    "loss_val_final": history.history["val_loss"][-1],
                    "r2_val_max": max(history.history["val_r2"]),
                    "r2_final": history.history["val_r2"][-1],
                    "ccc_val_max": max(history.history["ccc"]),
                    "ccc_val_final": history.history["ccc"][-1],
                    "epochs": n_epochs,
                    "batch": batch_size,
                },
                index=[Model_Num],
            )

            scores = scores.append(res)
            res.to_csv(os.path.join(run_path, str(Model_Num) + "_score.csv"))
            Model_Num += 1

    if not scores.empty:
        scores.to_csv(os.path.join(run_path, "all_scores.csv"))
