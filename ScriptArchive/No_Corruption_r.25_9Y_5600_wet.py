
from sklearn.model_selection import train_test_split
from monitoring import r2, ccc, eval_at_missing
from preprocessing import *
from modelConstrunction import *
import os
import random
import pickle
from analytics import Analyze


if __name__ == "__main__":

    ####################################################################################################################
    # PARSING
    ####################################################################################################################
    fix_seed = 36
    if fix_seed:
        np.random.seed(fix_seed)
        tf.random.set_seed(fix_seed)
        random.seed(fix_seed)
        os.environ["PYTHONHASHSEED"] = str(fix_seed)
    center_id = 5600
    center_radius = .25
    n_rows = None
    id_field = "STATIONS_ID"
    lat_field = "GEOGR_BREITE"
    lon_field = "GEOGR_LAENGE"
    stamp_field = "MESS_DATUM"
    field = "NIEDERSCHLAGSHOEHE"
    years = ["2010", "2011" ,"2012", "2013", "2014", "2015", "2016",
             "2017", '2018', "2019"]
    folder_path = "DWDData/RR"
    file_folder = "RR"
    subset_frame = corrupted_frame = corruption_mask = pd.DataFrame([])

    geo_frame = pd.read_parquet(os.path.join(folder_path, 'geoFrame.parquet'))
    IDs = get_geo_neighbors(
        center_id, geo_frame, center_radius, lat_field, lon_field
    )
    A = Analyze(run="dummy1/dummy2", RR_folder=file_folder)
    A.load_data(years, load_mode=[str(x) + '_RR' for x in IDs])

    # MISS DIST ANALYSIS AND REPRODUCTION
    #na_num, na_dist = get_miss_dist(m_frame)
    #complete_stamps = get_full_ts(sub_frame)
    #sub_frame, corr_frame, corr_mask = corrupt(
    #    sub_frame.loc[complete_stamps, :],
    #    lambda x: random_drop(x, (na_num, na_dist)),
    #    -1,
    #)

    subset_frame = A.X
    # filter out distorting Stations
    BAD_station_mask = subset_frame.isna().sum() > 50000
    BAD_stations = BAD_station_mask[BAD_station_mask].index
    subset_frame = subset_frame.drop(BAD_stations, 1)
    # filter out complete stations
    complete_ts = subset_frame.isna().sum(axis=1)==0
    complete_ts = complete_ts[complete_ts].index
    subset_frame = subset_frame.loc[complete_ts,:]
    # only wet days:
    subset_frame = subset_frame.loc[(subset_frame != 0).any(axis=1), :]
    #subset_frame = subset_frame.loc[subset_frame[str(center_id) +'_RR'] != 0, :]

    center_id = str(center_id) + '_RR'
    # no artificial missing values:
    corrupted_frame = subset_frame
    # dummy mask - no artificial missing values in this run:
    corruption_mask = pd.DataFrame(False, index=corrupted_frame.index, columns=corrupted_frame.columns)

    ####################################################################################################################
    # Normalization / Standardisation / Preparation
    ####################################################################################################################

    scales = subset_frame.max()
    scales[scales == 0] = 1

    train_percentage = 0.8
    test_percentage = 0.001
    x_train, x_test, y_train, y_test = train_test_split(
        scaler(scales, corrupted_frame, corruption_mask),
        subset_frame / scales,
        test_size=(1 - train_percentage),
        random_state=fix_seed,
    )


    x_test, x_val, y_test, y_val = train_test_split(
        x_test, y_test, test_size=(1 - test_percentage), random_state=fix_seed
    )

    x_test_i, x_val_i, y_test_i, y_val_i, x_train_i, y_train_i = (
        x_test.index,
        x_val.index,
        y_test.index,
        y_val.index,
        x_train.index,
        y_train.index,
    )
    x_test, x_val, x_train = (
        x_test.drop(center_id, 1),
        x_val.drop(center_id, 1),
        x_train.drop(center_id, 1),
    )
    y_test, y_val, y_train = y_test[center_id], y_val[center_id], y_train[center_id]
    ####################################################################################################################
    # Prepare Experiment Storage / doc Experiment scenario
    ####################################################################################################################

    Exp_folder = "Results/currentRun"
    if not os.path.exists(Exp_folder):
        os.mkdir(Exp_folder)

    ####################################################################################################################
    # Run
    ####################################################################################################################
    Model_Num = 0
    run_name = "No_Corruption_r.25_9Y_wet"
    run_path = os.path.join(Exp_folder, run_name)
    if not os.path.exists(run_path):
        os.mkdir(run_path)

    ########################################
    # Generate reconstruction Info for the run:
    ########################################
    info = pd.Series({"center_id": center_id,
                      "radius": center_radius,
                      "seed": fix_seed,
                      "years": years,
                      "corruption": 'none',
                      'ts_drop': 'any',
                      'Data': 'RR',
                      'Note': 'ONLY WET DAYS'})

    info.to_csv(os.path.join(run_path, "info.csv"))
    c_temp = corruption_mask.columns
    corruption_mask.columns = [str(x) for x in c_temp]
    corruption_mask.to_parquet(os.path.join(run_path, "miss_mask.parquet"))
    corruption_mask.columns = c_temp
    IDs.to_series().to_csv(os.path.join(run_path, "IDs.csv"), index=False)
    x_train_i.to_series().to_csv(os.path.join(run_path, "train_index.csv"), index=False)
    x_val_i.to_series().to_csv(
        os.path.join(run_path, "validation_index.csv"), index=False
    )
    x_test_i.to_series().to_csv(os.path.join(run_path, "test_index.csv"), index=False)
    pd.DataFrame(x_train).columns.to_series().to_csv(
        os.path.join(run_path, "IDx.csv"), index=False
    )
    pd.DataFrame(y_train).columns.to_series().to_csv(
        os.path.join(run_path, "IDy.csv"), index=False
    )
    scales.to_csv(os.path.join(run_path, "scales.csv"))

    n_inputs = x_train.shape[1]  # 318
    loss = "mean_squared_error"
    metrics = [ccc, "mae", r2]
    scores = pd.DataFrame([])
    moddels = [
        #[n_inputs, 1],
        #[n_inputs, str(0.5) + "_drop", 1],
        #[n_inputs, int(round(n_inputs / 2)), 1],
        #[n_inputs, str(0.5) + "_drop", int(round(n_inputs / 2)), str(0.5) + "_drop", 1],
        [n_inputs, int(round(n_inputs * 0.66)), int(round(n_inputs * 0.33)), 1],
        [
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 0.66)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.33)),
            str(0.5) + "_drop",
            1,
        ],
        [n_inputs, n_inputs, int(round(n_inputs * 0.66)), int(round(n_inputs * 0.33)), 1],
        [
            n_inputs,
            str(0.5) + "_drop",
            n_inputs,
            str(0.5) + "_drop",
            int(round(n_inputs * 0.66)),
            str(0.5) + "_drop",
            int(round(n_inputs * 0.33)),
            str(0.5) + "_drop",
            1,
        ],
    ]
    ####################################################################################################################
    # Training Model
    ####################################################################################################################

    for mod in moddels:
        for train_para in [(200, 20), (1000, 200), (2000, 2000)]:
            m = make_model(
                mod,
                input_dim=n_inputs,
                loss=loss,
                monitors=metrics,
                seed=fix_seed,
                optimizer=tf.keras.optimizers.Adam(lr=1e-3),
            )

            n_epochs = train_para[0]
            batch_size = train_para[1]
            history = m.fit(
                x_train,
                y_train,
                epochs=n_epochs,
                batch_size=batch_size,
                validation_data=(x_val, y_val),
            )

            with open(
                os.path.join(run_path, str(Model_Num) + "_history.pickle"), "wb"
            ) as f:
                pickle.dump(history.history, f)

            with open(
                os.path.join(run_path, str(Model_Num) + "_summary.txt"), "w"
            ) as fh:
                m.summary(print_fn=lambda x: fh.write(x + "\n"))

            m.save(os.path.join(run_path, str(Model_Num) + "_model"))

            res = pd.DataFrame(
                {
                    "loss_ratio": (history.history["val_loss"][-1])/(history.history["loss"][-1]),
                    "loss_val_min": min(history.history["val_loss"]),
                    "loss_val_final": history.history["val_loss"][-1],
                    "loss_train_final": history.history["loss"][-1],
                    "r2_val_max": max(history.history["val_r2"]),
                    "r2_val_final": history.history["val_r2"][-1],
                    "r2_train_final": history.history["r2"][-1],
                    "ccc_val_max": max(history.history["val_ccc"]),
                    "ccc_val_final": history.history["val_ccc"][-1],
                    "ccc_train_final": history.history["ccc"][-1],
                    "epochs": n_epochs,
                    "batch": batch_size,
                },
                index=[Model_Num],
            )

            scores = scores.append(res)
            scores.to_csv(os.path.join(run_path, "all_scores.csv"))
            res.to_csv(os.path.join(run_path, str(Model_Num) + "_score.csv"))
            Model_Num += 1
    if not scores.empty:
        scores.to_csv(os.path.join(run_path, "all_scores.csv"))


